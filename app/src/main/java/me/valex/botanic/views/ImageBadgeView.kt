package me.valex.botanic.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import me.valex.botanic.R

class ImageBadgeView : RelativeLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private var image: ImageView? = null
    private var badge: TextView? = null
    private var badgeFrame: View? = null

    private fun init() {
        LayoutInflater.from(context).inflate(R.layout.view_image_badge, this)

        image = findViewById(R.id.image) as ImageView
        badge = findViewById(R.id.badge) as TextView
        badgeFrame = findViewById(R.id.badgeFrame)
    }

    fun setImageResource(resId: Int) {
        image?.setImageResource(resId)
    }

    fun setTintColor(color: Int) {
        image?.setColorFilter(color)
    }

    fun setBadge(text: String) {
        if (text.isEmpty() || text == "0") {
            badgeFrame?.visibility = View.GONE
        }
        else {
            badgeFrame?.visibility = View.VISIBLE
            badge?.text = text
        }
    }
}
