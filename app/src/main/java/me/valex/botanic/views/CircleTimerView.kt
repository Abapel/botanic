package me.valex.botanic.views

import android.content.Context
import android.graphics.Color
import android.os.Handler
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.RelativeLayout
import android.widget.TextView
import me.valex.botanic.R
import me.valex.botanic.extensions.TIMER_STATE_PAUSED
import me.valex.botanic.extensions.TIMER_STATE_RUNNING
import me.valex.botanic.extensions.TIMER_STATE_STOPPED
import android.view.Gravity
import me.valex.botanic.extensions.log


/**
 * Created by alex on 03.03.17.
 */

class CircleTimerView(context: Context, attrs: AttributeSet?, defStyle: Int = 0) : RelativeLayout(context, attrs, defStyle) {
    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, 0)
    constructor(context: Context) : this(context, null, defStyle = 0)

    private var running: Boolean = false

    var flexibleSpace: View? = null
    var textTime: TextView? = null
    var container1: RelativeLayout? = null
    val _inflater: LayoutInflater = LayoutInflater.from(context)
    var _clockFaceViewHolders: ArrayList<ClockFaceViewHolder>
    val _clockFaceCount: Int

    init {
        _inflater.inflate(R.layout.view_circle_timer, this)
        textTime = findViewById(R.id.textTime) as TextView
        container1 = findViewById(R.id.container1) as RelativeLayout
        flexibleSpace = findViewById(R.id.flexibleSpace)

        _clockFaceCount = 180
        _clockFaceViewHolders = ArrayList<ClockFaceViewHolder>()
        initClockFace()
    }

    class ClockFaceViewHolder(view: View) {
        val line: View = view.findViewById(R.id.line)
    }

    fun setState(state: Int) {
        when (state) {
            TIMER_STATE_RUNNING -> {
                setRunning_(true)
            }
            TIMER_STATE_PAUSED -> {
                setRunning_(false)
            }
            TIMER_STATE_STOPPED -> {
                setRunning_(false)
            }
        }
    }

    val h = Handler()

    fun setRunning_(running: Boolean) {
        this.running = running
        if (running) {
            start()
        }
        else {
            stop()
        }
    }

    val maxTextScale = 1.6F
    val minTextScale = 1F


    fun initClockFace() {
        val params = LayoutParams(5, LayoutParams.MATCH_PARENT)
        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE)

        val step: Float = 360F / _clockFaceCount

        for (i in 0.._clockFaceCount-1) {
            val view = _inflater.inflate(R.layout.view_clock_face_item, null, false)
            view.layoutParams = params
            view.rotation = i * step
            container1?.addView(view)
            _clockFaceViewHolders.add(ClockFaceViewHolder(view))
        }
    }

    fun setTimerScale(scale: Float) {
        flexibleSpace?.scaleY = scale

        val delta = maxTextScale - minTextScale
        val textScale = minTextScale + delta * scale
        textTime?.scaleX = textScale
        textTime?.scaleY = textScale
    }

    fun start() {
        if (running && rotateMicroCircle()) {
            h.postDelayed({
                start()
            }, 1500L)
        }
    }

    fun stop() {
        running = false
        h.removeCallbacksAndMessages(null)
    }

    fun setTextSize(size: Float) {
        textTime?.textSize = size
    }

    fun setText(text: String) {
        textTime?.text = text
    }

    fun setTime(timeMillis: Long) {
        val allSecs = timeMillis / 1000
        val allMins = allSecs / 60
        val hrs = allMins / 60
        val mins = allMins % 60
        val secs = allSecs % 60

        log(secs)

        _setSecondsClockFace(timeMillis)
    }

    val minScale = 1.0F
    val maxScale = 3.0F

    fun _setSecondsClockFace(millis: Long) {

        val msFurther = 0
        val index = _clockFaceCount.toFloat() / 60000 * ((millis + msFurther) % 60000)
        val id = index.toInt()

        for (i in 0.._clockFaceViewHolders.size-1) {
            if (i == id && _clockFaceViewHolders[i].line.animation?.hasEnded() ?: true) {
                _clockFaceViewHolders[i].line.animate().scaleY(maxScale).setDuration(500L).start()
            }
            else if (_clockFaceViewHolders[i].line.scaleY != minScale && _clockFaceViewHolders[i].line.animation?.hasEnded() ?: true) {
                _clockFaceViewHolders[i].line.animate().scaleY(minScale).setDuration(500L).start()
            }
        }
    }

    fun rotateMicroCircle(): Boolean {
        return false
    }

    fun setControlsAlpha(alpha: Float) {
        flexibleSpace?.alpha = alpha
    }
}