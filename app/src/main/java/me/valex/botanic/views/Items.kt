package me.valex.botanic.views

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import me.valex.botanic.R
import me.valex.botanic.databinding.ItemProjectBinding
import me.valex.botanic.databinding.ItemSessionBinding
import me.valex.botanic.models.Project
import me.valex.botanic.models.Session

/**
 * Created by alex on 03.03.17.
 */

class Items {

    companion object {

        fun getProjectView(context: Context, project: Project): View {
            val binding = DataBindingUtil.inflate<ItemProjectBinding>(LayoutInflater.from(context), R.layout.item_project, null, false)
            binding.project = project
            return binding.root
        }

        fun getSessionView(context: Context, session: Session): View {
            val binding = DataBindingUtil.inflate<ItemSessionBinding>(LayoutInflater.from(context), R.layout.item_session, null, false)
            binding.session = session
            return binding.root
        }
    }
}