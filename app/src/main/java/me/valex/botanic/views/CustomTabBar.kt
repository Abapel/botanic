package me.valex.botanic.views

import android.content.Context
import android.databinding.DataBindingUtil
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import me.valex.botanic.R
import me.valex.botanic.databinding.ViewCustomToolbarBinding
import me.valex.botanic.extensions.log
import me.valex.botanic.viewmodels.*

class CustomTabBar : RelativeLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private var binding: ViewCustomToolbarBinding? = null

    private fun init() {
        val inflater = LayoutInflater.from(context)
        binding = DataBindingUtil.inflate<ViewCustomToolbarBinding>(inflater, R.layout.view_custom_toolbar, this, true)
    }

    fun initWithModel(model: ProjectViewModel) {
        binding?.model = model

        findViewById(R.id.item1).setOnClickListener {
            model.onMenuItemSelected.invoke(MENU_ITEM_CHAT)
        }
        findViewById(R.id.item2).setOnClickListener {
            model.onMenuItemSelected.invoke(MENU_ITEM_PIN)
        }
        findViewById(R.id.item3).setOnClickListener {
            model.onMenuItemSelected.invoke(MENU_ITEM_TASKS)
        }
        findViewById(R.id.item4).setOnClickListener {
            model.onMenuItemSelected.invoke(MENU_ITEM_TEAM)
        }
        findViewById(R.id.item5).setOnClickListener {
            model.onMenuItemSelected.invoke(MENU_ITEM_GRAPH)
        }
    }
}