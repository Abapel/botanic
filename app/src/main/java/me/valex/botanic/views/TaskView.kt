package me.valex.botanic.views

import android.content.Context
import android.databinding.DataBindingUtil
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import me.valex.botanic.R
import me.valex.botanic.databinding.ItemTaskBinding
import me.valex.botanic.viewmodels.TaskViewModel

/**
 * Created by alex on 24.05.17.
 */

class TaskView : RelativeLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private var binding: ItemTaskBinding? = null

    private fun init() {
        val inflater = LayoutInflater.from(context)
        binding = DataBindingUtil.inflate<ItemTaskBinding>(inflater, R.layout.item_task, this, true)
    }

    fun initWithModel(model: TaskViewModel) {
        binding?.model = model
    }
}