package me.valex.botanic.views

import android.content.Context
import android.databinding.DataBindingUtil
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import me.valex.botanic.R
import me.valex.botanic.databinding.ItemTaskBinding
import me.valex.botanic.databinding.ViewCustomToolbarBinding
import me.valex.botanic.databinding.ViewTasksScrollviewBinding
import me.valex.botanic.models.Session
import me.valex.botanic.models.Task
import me.valex.botanic.viewmodels.ProjectViewModel
import me.valex.botanic.viewmodels.TaskViewModel

/**
 * Created by alex on 24.05.17.
 */

class TasksScrollView : RelativeLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    var binding: ViewTasksScrollviewBinding? = null

    fun init() {
        val inflater = LayoutInflater.from(context)
        binding = DataBindingUtil.inflate<ViewTasksScrollviewBinding>(inflater, R.layout.view_tasks_scrollview, this, true)
        contentView = findViewById(R.id.content) as LinearLayout
    }

    val inflater: LayoutInflater = LayoutInflater.from(context)
    var contentView: LinearLayout? = null

    private var projectModel: ProjectViewModel? = null
    fun _setProjectModel(project: ProjectViewModel) {
        projectModel = project
        binding?.project = project
    }
    var session: Session? = null
    var items: ArrayList<Task> = ArrayList()

    fun _setItems(newItems: ArrayList<Task>) {

        newItems.removeAll(items)
        items.addAll(newItems)

        items.forEach { task ->
            val binding: ItemTaskBinding?
            val convertView: View? = null

            if (convertView == null) {
                binding = DataBindingUtil.inflate<ItemTaskBinding>(inflater, R.layout.item_task, null, false)
                binding.root.tag = binding
            }
            else {
                binding = convertView.tag as ItemTaskBinding
            }

            binding?.task = task
            binding?.model = TaskViewModel(context, task)

            binding?.root?.findViewById(R.id.btnAddTask)?.setOnClickListener {
                projectModel?.onStartSession(task)
            }

            contentView?.addView(binding?.root)
        }
    }
}