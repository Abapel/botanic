package me.valex.botanic.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import me.valex.botanic.R
import me.valex.botanic.databinding.ItemProjectBinding
import me.valex.botanic.databinding.ItemTaskBinding
import me.valex.botanic.models.Project
import me.valex.botanic.models.Task
import java.util.*

/**
 * Created by alex on 27.02.17.
 */

class TasksAdapter(val context: Context,
                   var items: ArrayList<Task> = ArrayList()) : BaseAdapter() {

    val inflater: LayoutInflater = LayoutInflater.from(context)

    fun updateItems(items: ArrayList<Task>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): Task = items[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = items.size

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val binding: ItemTaskBinding?

        if (convertView == null) {
            binding = DataBindingUtil.inflate<ItemTaskBinding>(inflater, R.layout.item_task, parent, false)
            binding.root.tag = binding
        }
        else {
            binding = convertView.tag as ItemTaskBinding
        }

        val item = getItem(position)
        binding?.task = item

        return binding?.root
    }
}