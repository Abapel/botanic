package me.valex.botanic.adapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.support.annotation.ColorInt;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Map;

import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import me.valex.botanic.R;
import me.valex.botanic.activities.ProjectActivity;
import me.valex.botanic.dialogs.ChatMessageDialog;
import me.valex.botanic.extensions.CircleTransform;
import me.valex.botanic.extensions.CommonKt;
import me.valex.botanic.models.Message;
import me.valex.botanic.models.Project;
import me.valex.botanic.models.Session;
import me.valex.botanic.models.Task;
import me.valex.botanic.network.Api;
import me.valex.botanic.viewmodels.ProjectListViewModel;
import me.valex.botanic.viewmodels.LoginViewModel;
import me.valex.botanic.viewmodels.ProjectViewModel;
import me.valex.botanic.viewmodels.TaskViewModel;
import me.valex.botanic.views.CircleTimerView;
import me.valex.botanic.views.CustomTabBar;
import me.valex.botanic.views.ImageBadgeView;
import me.valex.botanic.views.Items;
import me.valex.botanic.views.TasksScrollView;

import static android.R.attr.width;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;
import static me.valex.botanic.R.id.listView;


/**
 * Created by alex on 03.02.17.
 */

public class BindingAdapters {

    @BindingAdapter("elevation")
    public static void bindElevation(View view, int elevation) {
        ViewCompat.setElevation(view, elevation);
    }

    @BindingAdapter("tintColor")
    public static void bindTintColor(ImageBadgeView view, int color) {
        view.setTintColor(color);
    }

    @BindingAdapter("imageResource")
    public static void bindImage(ImageBadgeView view, int resId) {
        view.setImageResource(resId);
    }

    @BindingAdapter("imageResource")
    public static void bindImage(ImageView view, int resId) {
        view.setImageResource(resId);
    }

    @BindingAdapter("badge")
    public static void bindBadge(ImageBadgeView view, String badge) {
        view.setBadge(badge);
    }

    @BindingAdapter("projectItems")
    public  static void bindProjectItems(LinearLayout parent, ArrayList<Project> projects) {
        parent.removeAllViews();

        if (projects != null) {
            for (Project each : projects) {
                View view = Items.Companion.getProjectView(parent.getContext(), each);
                parent.addView(view);
            }
        }
    }

    private static Handler fabHandler;
    private static Handler getFabHandler() {
        if (fabHandler == null) {
            fabHandler = new Handler();
        }
        return fabHandler;
    }

    @BindingAdapter("fabSrc")
    public static void bindFabSrc(final FloatingActionButton fab, int state) {
        Boolean isRunning = state == Session.Companion.getSTATE_RUNNING();
        final int resId = isRunning ? R.drawable.ic_pause_white : R.drawable.ic_start_white;
        float rotation = isRunning ? 180F : 0F;
        long duration = 200L;

        fab.animate().rotation(rotation).setDuration(duration).start();
        getFabHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fab.setImageResource(resId);
            }
        }, duration / 2);
        fab.setImageResource(resId);
    }

    @BindingAdapter("bg")
    public static void bindSessions(View view, long color) {
        view.setBackgroundColor((int)color);
    }

    /*
    @BindingAdapter("sessions")
    public static void bindSessions(ListView listView, ArrayList<Session> sessions) {
        if (sessions == null)
            return;

        if (listView.getAdapter() == null) {
            SessionsAdapter adapter = new SessionsAdapter(listView.getContext(), sessions);
            listView.setAdapter(adapter);
        }
        else {
            ((SessionsAdapter) listView.getAdapter()).updateItems(sessions);
        }
    }
    */

    @BindingAdapter("tasks")
    public static void bindTasks(TasksScrollView scrollView, Map<String, Task> tasks) {
        if (tasks == null)
            return;

        scrollView._setItems(new ArrayList<>(tasks.values()));
    }

    @BindingAdapter("startTask")
    public static void bindStartTask(final FloatingActionButton fab, final TaskViewModel task) {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (task != null) {
                    task.onStartTask(fab);
                }
            }
        });
    }

    @BindingAdapter("background")
    public static void bindBackground(ImageView imageView, String color) {
        try {
            imageView.setBackgroundColor(Color.parseColor(color));
        }
        catch (Exception e) {}
    }

    @BindingAdapter("tintColor")
    public static void bindTintColor(ImageBadgeView view, String color) {
        try {
            view.setTintColor(Color.parseColor(color));
        }
        catch (Exception e) {}
    }

    @BindingAdapter("tintColor")
    public static void bindTintColor(ImageView view, String color) {
        try {
            view.setColorFilter(Color.parseColor(color));
        }
        catch (Exception e) {}
    }

    @BindingAdapter("textColorHint")
    public static void bindTintColor(TextInputLayout view, String colorString) {
        try {
            int color = Color.parseColor("#000000");
            view.getEditText().setHighlightColor(color);
            view.getEditText().setHintTextColor(color);

            Drawable background = view.getEditText().getBackground();
            DrawableCompat.setTint(background, color);
            view.getEditText().setBackground(background);
        }
        catch (Exception e) {}
    }

    @BindingAdapter("clickEvents")
    public static void bindClickEvents(final ObservableListView listView, final ProjectViewModel model) {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                final Message clickedMessage = (Message) listView.getAdapter().getItem(position);

                ChatMessageDialog dialog = new ChatMessageDialog(new Function0<Unit>() {
                    @Override
                    public Unit invoke() {
                        model.setCurrentMessage(clickedMessage);
                        model._setCurrentMessage(clickedMessage.getMessage());
                        model.setEditingMessage(true);
                        return null;
                    }
                }, new Function0<Unit>() {
                    @Override
                    public Unit invoke() {
                        Api.Companion.removeMessage(listView.getContext(), model.getProject().getChatId(), clickedMessage);
                        return null;
                    }
                });
                FragmentManager fm = ((Activity) listView.getContext()).getFragmentManager();
                dialog.show(fm, "ChatMessageDialog");
            }
        });
    }

    @BindingAdapter("messages")
    public static void bindMessages(final com.github.ksoichiro.android.observablescrollview.ObservableListView listView, ArrayList<Message> messages) {
        if (messages == null)
            return;

        if (listView.getAdapter() == null) {
            ChatListAdapter adapter = new ChatListAdapter(listView.getContext(), messages);
            listView.setAdapter(adapter);

            View paddingView = new View(listView.getContext());
            AbsListView.LayoutParams lp = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, listView.getContext().getResources().getDimensionPixelSize(R.dimen.flexible_space_image_height));
            paddingView.setLayoutParams(lp);

            // This is required to disable header's list selector effect
            paddingView.setClickable(true);

            listView.addHeaderView(paddingView);
        }
        else {
            ((ChatListAdapter)((HeaderViewListAdapter)listView.getAdapter()).getWrappedAdapter()).updateItems(messages);
        }
    }

    @BindingAdapter("pager")
    public static void bindPager(final ViewPager pager, final ProjectViewModel model) {
        Context context = pager.getContext();
        if (pager.getAdapter() == null) {
            ProjectPagerAdapter adapter = new ProjectPagerAdapter(context, ((AppCompatActivity) context).getSupportFragmentManager(), model);
            pager.setAdapter(adapter);
            pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    int scrollX = pager.getWidth() * position + positionOffsetPixels;
                    model.setScrollX(scrollX);
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            model.setOnMenuItemSelected(new Function1<Integer, Unit>() {
                @Override
                public Unit invoke(Integer menuItem) {
                    pager.setCurrentItem(menuItem);
                    return null;
                }
            });
        }
    }

    @BindingAdapter("width")
    public static void bindWidth(RelativeLayout view, int width) {
        view.setLayoutParams(new RelativeLayout.LayoutParams(width, RelativeLayout.LayoutParams.MATCH_PARENT));
    }

    @BindingAdapter("translationX")
    public static void bindTranslationX(RelativeLayout view, int translationX) {
        view.setTranslationX(translationX);
    }

    @BindingAdapter("circleUrl")
    public static void bindCircleUrl(ImageView imageView, String url) {

        //todo remove hardcoded url
        url = "https://pp.userapi.com/c10462/u14116007/128183484/x_06ea8ea8.jpg";

        if (!url.isEmpty()) {
            Glide
                .with(imageView.getContext())
                .load(url)
                .transform(new CircleTransform(imageView.getContext()))
                .into(imageView);
        }
    }

    @BindingAdapter("url")
    public static void bindUrl(ImageView imageView, String url) {
        if (!url.isEmpty()) {
            Glide
                .with(imageView.getContext())
                .load(url)
                .into(imageView);
        }
    }

    @BindingAdapter("sessions")
    public static void bindSessions(com.github.ksoichiro.android.observablescrollview.ObservableListView listView, Map<String, Session> sessions) {
        if (sessions == null)
            return;

        if (listView.getAdapter() == null) {

            SessionsAdapter adapter = new SessionsAdapter(listView.getContext(), new ArrayList<>(sessions.values()));
            listView.setAdapter(adapter);

            View paddingView = new View(listView.getContext());
            AbsListView.LayoutParams lp = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, listView.getContext().getResources().getDimensionPixelSize(R.dimen.flexible_space_image_height));
            paddingView.setLayoutParams(lp);

            // This is required to disable header's list selector effect
            paddingView.setClickable(true);

            listView.addHeaderView(paddingView);
        }
        else {
            ((SessionsAdapter) listView.getAdapter()).updateItems(new ArrayList<>(sessions.values()));
        }
    }

    @BindingAdapter("model")
    public static void bindModel(CustomTabBar tabBar, ProjectViewModel model) {
        tabBar.initWithModel(model);
    }

    @BindingAdapter("hideKeyboard")
    public static void bindHideKeyboard(final View view, boolean hide) {
        if (hide) {
            setupUI(view);
        }
    }

    private static void setupUI(final View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(view.getContext());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Context activity) {
        try {
            if (activity instanceof AppCompatActivity) {
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(((AppCompatActivity)activity).getCurrentFocus().getWindowToken(), 0);
            }
        }
        catch (Exception ignored) {}
    }

    @BindingAdapter("circleColor")
    public static void bindCircleColor(View view, long color) {
        if (color != 0) {
            GradientDrawable shapeBg = (GradientDrawable) view.getBackground();
            shapeBg.setColor((int) color);
        }
    }

    @BindingAdapter("timerScale")
    public static void bindTimerScale(CircleTimerView view, float scale) {
        view.setTimerScale(scale);
    }

    @BindingAdapter("timerTime")
    public static void bindTimerText(CircleTimerView view, long time) {
        view.setTime(time);
        view.setText(CommonKt.formatTime(time));
    }

    @BindingAdapter("fabColor")
    public static void bindFabColor(FloatingActionButton fab, int color) {
        fab.setBackgroundTintList(ColorStateList.valueOf(color));
    }

    @BindingAdapter("textColorBounce")
    public static void textColorBounce(TextView textView, int color) {
        CommonKt.bounceTimer(textView, color);
    }

    @BindingAdapter("src")
    public static void src(ImageView imageView, Session session) {
        if (session != null) {
            imageView.setImageResource(session._isRunning() ? R.drawable.ic_pause : R.drawable.ic_play);
        }
    }

    @BindingAdapter("text")
    public static void bindText(CircleTimerView timerView, String text) {
        timerView.setText(text);
    }

    @BindingAdapter("textSize")
    public static void bindText(CircleTimerView timerView, float textSize) {
        timerView.setTextSize(textSize);
    }

    @BindingAdapter("state")
    public static void bindText(CircleTimerView timerView, int state) {
        timerView.setState(state);
    }

    @BindingAdapter("onAddProject")
    public static void onAddTask(final FloatingActionButton fab, final ProjectListViewModel model) {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                model.onAddProject(fab);
            }
        });
    }

    @BindingAdapter("onAddTask")
    public static void onAddTask(final FloatingActionButton fab, final ProjectViewModel model) {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                model.onAddTask();
            }
        });
    }

    @BindingAdapter("onStopSession")
    public static void bindOnStopSession(final FloatingActionButton fab, final ProjectViewModel model) {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                model.onStopSession();
            }
        });
    }

    final static long duration = 300L;
    static Handler _handler = new Handler();

    @BindingAdapter("visibility")
    public static void bindVisibility(final FloatingActionButton fab, final boolean isVisible) {
        float scale = isVisible ? 1F : 0F;
        float alpha = isVisible ? 1F : 0F;
        fab.setVisibility(View.VISIBLE);
        fab.animate().scaleX(scale).scaleY(scale).alpha(alpha).setDuration(duration).start();
        _handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (fab != null) {
                    fab.setVisibility(isVisible ? View.VISIBLE : View.GONE);
                }
            }
        }, duration);
    }

    @BindingAdapter("sessionCardVisibility")
    public static void sessionCardVisibility(final RelativeLayout fab, final boolean isVisible) {
        float scale = isVisible ? 1F : 0F;
        float alpha = isVisible ? 1F : 0F;

        if (isVisible && fab.getTag() != null && fab.getTag() instanceof String) {
            fab.setTag(null);
        }
        else {
            fab.setVisibility(View.VISIBLE);
        }

        fab.animate().scaleX(scale).scaleY(scale).alpha(alpha).setDuration(duration).start();
        _handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (fab != null) {
                    fab.setVisibility(isVisible ? View.VISIBLE : View.GONE);
                }
            }
        }, duration);
    }

    @BindingAdapter("tasksContentVisibility")
    public static void tasksContentVisibility(final LinearLayout fab, final boolean isVisible) {
        float scale = isVisible ? 1F : 3F;
        float alpha = isVisible ? 1F : 0F;
        fab.animate().scaleX(scale).scaleY(scale).alpha(alpha).start();
    }

    @BindingAdapter("onClick")
    public static void bindTaskList(final FloatingActionButton fab, final ProjectViewModel model) {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                model.onAddTask();
            }
        });
    }

    @BindingAdapter("onScroll")
    public  static void bindOnScroll(final ObservableListView listView, final ProjectViewModel model) {
        listView.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
            @Override
            public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
                model.onScrollChanged(scrollY, firstScroll, dragging);
            }

            @Override
            public void onDownMotionEvent() {

            }

            @Override
            public void onUpOrCancelMotionEvent(ScrollState scrollState) {

            }
        });
    }

    @BindingAdapter("onScroll")
    public  static void bindOnScroll(final ObservableScrollView view, final ProjectViewModel model) {
        if (model == null) {
            return;
        }

        view.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
            @Override
            public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
                model.onScrollChanged(scrollY, firstScroll, dragging);
            }

            @Override
            public void onDownMotionEvent() {

            }

            @Override
            public void onUpOrCancelMotionEvent(ScrollState scrollState) {

            }
        });
    }

    @BindingAdapter("screenHeight")
    public static void bindScreenHeight(RelativeLayout view, int count) {

        Resources res = view.getContext().getResources();
        int itemHeight = (int) res.getDimension(R.dimen.item_task_height);
        int totalHeightItems = itemHeight * (count + 2);

        Display display = ((Activity) view.getContext()).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int screenHeight = size.y;

        int flexibleSpace = (int) res.getDimension(R.dimen.flexible_space_image_height);
        int tabBarSize = (int) res.getDimension(R.dimen.tab_bar_height);
        int totalScreenHeight = screenHeight + flexibleSpace;

        int result = totalHeightItems;
        if (totalScreenHeight > result)
            result = totalScreenHeight;

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, result);
        view.setLayoutParams(params);
    }

    @BindingAdapter("screenHeight")
    public static void bindScreenHeight(RelativeLayout view, boolean bind) {

        Resources res = view.getResources();

        Display display = ((Activity) view.getContext()).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int screenHeight = size.y;

        int flexibleSpace = (int) res.getDimension(R.dimen.flexible_space_image_height);
        int tabBarSize = (int) res.getDimension(R.dimen.tab_bar_height);
        int totalScreenHeight = screenHeight + flexibleSpace - 2 * tabBarSize;

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, totalScreenHeight);
        view.setLayoutParams(params);
    }

    @BindingAdapter("project")
    public static void bindProject(TasksScrollView scrollView, ProjectViewModel project) {
        scrollView._setProjectModel(project);
    }

    @BindingAdapter("session")
    public static void bindSession(TasksScrollView scrollView, Session session) {
        scrollView.setSession(session);
    }

    @BindingAdapter("items")
    public static void bindProjectsList(final ListView listView, final ArrayList<Project> projects) {
        if (projects == null)
            return;

        if (listView.getAdapter() == null) {
            ProjectsAdapter adapter = new ProjectsAdapter(listView.getContext(), projects);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Project clickedItem = ((ProjectsAdapter) listView.getAdapter()).getItem(i);
                    ProjectActivity.Companion.startIntent(listView.getContext(), clickedItem);
                }
            });
        }
        else {
            ((ProjectsAdapter) listView.getAdapter()).updateItems(projects);
        }
    }

    @BindingAdapter("usernameChanged")
    public static void bindUsernameChanged(final EditText editText, final LoginViewModel model) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                model.setUsername_(charSequence.toString());
            }
        });
    }

    @BindingAdapter("emailChanged")
    public static void bindEmailChanged(final EditText editText, final LoginViewModel model) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                model.setEmail_(charSequence.toString());
            }
        });
    }

    @BindingAdapter("passwordChanged")
    public static void bindPasswordChanged(final EditText editText, final LoginViewModel model) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                model.setPassword_(charSequence.toString());
            }
        });
    }

    @BindingAdapter("messageChanged")
    public static void bindMessageChanged(final EditText editText, final ProjectViewModel model) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                model._setCurrentMessage(charSequence.toString());
            }
        });
    }

    public static ColorStateList getColors(int color) {
        int[][] states = new int[][] {
                new int[] { android.R.attr.state_enabled}, // enabled
                new int[] {-android.R.attr.state_enabled}, // disabled
                new int[] {-android.R.attr.state_checked}, // unchecked
                new int[] { android.R.attr.state_pressed}  // pressed
        };

        int[] colors = new int[] {
                color,
                color,
                color,
                color
        };

        return new ColorStateList(states, colors);
    }

    @BindingAdapter("inputColor")
    public static void bindInputColor(final EditText editText, final int color) {
        setInputTextLayoutColor(editText, color);
    }

    private static void setInputTextLayoutColor(EditText editText, @ColorInt int color) {
        editText.getBackground().mutate().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        TextInputLayout til = (TextInputLayout) editText.getParent().getParent();
        try {
            Field fDefaultTextColor = TextInputLayout.class.getDeclaredField("mDefaultTextColor");
            fDefaultTextColor.setAccessible(true);
            fDefaultTextColor.set(til, new ColorStateList(new int[][]{{0}}, new int[]{ color }));

            Field fFocusedTextColor = TextInputLayout.class.getDeclaredField("mFocusedTextColor");
            fFocusedTextColor.setAccessible(true);
            fFocusedTextColor.set(til, new ColorStateList(new int[][]{{0}}, new int[]{ color }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BindingAdapter("alpha")
    public static void bindAlpha(CircleTimerView view, float alpha) {
        view.setControlsAlpha(alpha);
    }

    @BindingAdapter("state")
    public static void bindState(CircleTimerView view, int state) {
        view.setState(state);
    }
}
