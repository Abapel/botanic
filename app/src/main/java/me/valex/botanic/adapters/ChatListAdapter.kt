package me.valex.botanic.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import me.valex.botanic.R
import me.valex.botanic.databinding.ItemMessageBinding
import me.valex.botanic.models.Message

/**
 * Created by alex on 14.05.17.
 */

class ChatListAdapter(val context: Context, var items: ArrayList<Message> = ArrayList()) : BaseAdapter() {

    val inflater: LayoutInflater = LayoutInflater.from(context)

    init {
        items.reverse()
    }

    fun updateItems(items: ArrayList<Message>) {
        items.reverse()
        this.items = items
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): Message = items[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = items.size

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val binding: ItemMessageBinding

        if (convertView == null) {
            binding = DataBindingUtil.inflate<ItemMessageBinding>(inflater, R.layout.item_message, parent, false)
            binding.root.tag = binding
        }
        else {
            binding = convertView.tag as ItemMessageBinding
        }

        binding.message = getItem(position)
        return binding?.root
    }
}
