package me.valex.botanic.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import me.valex.botanic.fragments.TaskSettingsFragment
import me.valex.botanic.fragments.TaskSummaryFragment
import me.valex.botanic.fragments.SessionTimerFragment
import me.valex.botanic.models.Project
import me.valex.botanic.models.Session

open class MyPagerAdapter(fragmentManager: FragmentManager, val project: Project, var session: Session) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {

        when (position) {
            0 -> return SessionTimerFragment(project, session)
            1 -> return TaskSettingsFragment()
            2 -> return TaskSummaryFragment()
            else -> return Fragment()
        }
    }

    override fun getCount(): Int = 3
}