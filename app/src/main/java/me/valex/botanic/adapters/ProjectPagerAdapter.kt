package me.valex.botanic.adapters

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import me.valex.botanic.R
import me.valex.botanic.fragments.TasksFragment
import me.valex.botanic.fragments.ProjectTeamFragment
import android.view.ViewGroup
import me.valex.botanic.fragments.ChatFragment
import me.valex.botanic.viewmodels.ProjectViewModel


/**
 * Created by alex on 09.02.17.
 */

class ProjectPagerAdapter(val context: Context, fm: FragmentManager, val model: ProjectViewModel?) : FragmentStatePagerAdapter(fm) {

    override fun getCount(): Int = 5

    override fun getItem(position: Int): Fragment {
        when (position) {

            0 -> return ChatFragment(model)

            1 -> return ProjectTeamFragment(model)

            2 -> return TasksFragment(model)

            else -> return ProjectTeamFragment(model)
        }
    }

    override fun getPageTitle(position: Int): CharSequence {
        when (position) {

            0 -> return context.getString(R.string.activity)

            1 -> return context.getString(R.string.tasks)

            else -> return context.getString(R.string.details)
        }
    }

    override fun finishUpdate(container: ViewGroup?) {
        try {
            super.finishUpdate(container)
        } catch (nullPointerException: NullPointerException) {
            println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate")
        }

    }
}