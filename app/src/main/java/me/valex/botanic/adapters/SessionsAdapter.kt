package me.valex.botanic.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import me.valex.botanic.R
import me.valex.botanic.databinding.ItemSessionBinding
import me.valex.botanic.models.Session
import me.valex.botanic.views.CircleTimerView

class SessionsAdapter(val context: Context, var items: ArrayList<Session> = ArrayList()) : BaseAdapter() {

    val inflater: LayoutInflater = LayoutInflater.from(context)

    init {
        items.reverse()
    }

    fun updateItems(items: ArrayList<Session>) {
        items.reverse()
        this.items = items
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): Session = items[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = items.size

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val binding: ItemSessionBinding

        if (convertView == null) {
            binding = DataBindingUtil.inflate<ItemSessionBinding>(inflater, R.layout.item_session, parent, false)
            binding.root.tag = binding
        }
        else {
            binding = convertView.tag as ItemSessionBinding
        }

        val circle = binding.root.findViewById(R.id.timerView) as CircleTimerView;
        circle.stop()

        val item = getItem(position)
        binding?.session = item

        return binding?.root
    }
}
