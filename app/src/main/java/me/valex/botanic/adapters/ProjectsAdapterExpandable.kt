package me.valex.botanic.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.valex.botanic.R
import me.valex.botanic.databinding.ItemProjectBinding
import me.valex.botanic.databinding.ItemTaskBinding
import me.valex.botanic.models.Project
import me.valex.botanic.models.Task
import me.valex.botanic.views.AnimatedExpandableListView
import java.util.*

class ProjectsAdapterExpandable(val context: Context,
                      var projects: ArrayList<Project>) : AnimatedExpandableListView.AnimatedExpandableListAdapter() {

    val inflater: LayoutInflater
    init {
        inflater = LayoutInflater.from(context)
    }

    fun updateItems(projects: ArrayList<Project>) {
        this.projects = projects
        notifyDataSetChanged()
    }

    override fun getGroupCount(): Int = projects.size

    override fun getGroup(groupPosition: Int): Project = projects[groupPosition]

    override fun getChild(groupPosition: Int, childPosition: Int): Task? = null //projects[groupPosition].tasks[childPosition]

    override fun getGroupId(groupPosition: Int): Long = groupPosition.toLong()

    override fun getChildId(groupPosition: Int, childPosition: Int): Long = childPosition.toLong()

    override fun isChildSelectable(p0: Int, p1: Int): Boolean = false

    override fun hasStableIds(): Boolean = false

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View? {
        val binding: ItemProjectBinding?
        if (convertView == null) {
            binding = DataBindingUtil.inflate<ItemProjectBinding>(inflater, R.layout.item_project, parent, false)
            binding.root.tag = binding
        }
        else {
            binding = convertView.tag as ItemProjectBinding
        }

        binding?.project = getGroup(groupPosition)
        return binding?.root
    }

    override fun getRealChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View? {
        val binding: ItemTaskBinding?
        if (convertView == null) {
            binding = DataBindingUtil.inflate<ItemTaskBinding>(inflater, R.layout.item_task, parent, false)
            binding.root.tag = binding
        }
        else {
            binding = convertView.tag as ItemTaskBinding
        }

        binding?.task = getChild(groupPosition, childPosition)
        return binding?.root
    }

    override fun getRealChildrenCount(groupPosition: Int): Int = projects[groupPosition].tasks.size
}