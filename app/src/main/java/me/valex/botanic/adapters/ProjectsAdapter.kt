package me.valex.botanic.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import me.valex.botanic.R
import me.valex.botanic.databinding.ItemProjectBinding
import me.valex.botanic.databinding.ItemTaskBinding
import me.valex.botanic.models.Project
import me.valex.botanic.models.Task
import java.util.*


/**
 * Created by alex on 04.02.17.
 */

class ProjectsAdapter(val context: Context,
                      var items: ArrayList<Project> = ArrayList()) : BaseAdapter() {

    val inflater: LayoutInflater = LayoutInflater.from(context)

    fun updateItems(items: ArrayList<Project>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): Project = items[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = items.count()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val binding: ItemProjectBinding?

        if (convertView == null) {
            binding = DataBindingUtil.inflate<ItemProjectBinding>(inflater, R.layout.item_project, parent, false)
            binding.root.tag = binding
        }
        else {
            binding = convertView.tag as ItemProjectBinding
        }

        val item = getItem(position)
        binding?.project = item

        return binding?.root
    }
}