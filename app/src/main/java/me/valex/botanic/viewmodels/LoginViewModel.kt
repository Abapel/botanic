package me.valex.botanic.viewmodels

import android.content.Context
import android.content.Intent
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseUser
import me.valex.botanic.BR
import me.valex.botanic.R
import me.valex.botanic.activities.LoginActivity
import me.valex.botanic.activities.MainActivity
import me.valex.botanic.activities.ProfileActivity
import me.valex.botanic.extensions.*
import me.valex.botanic.models.Database
import me.valex.botanic.models.Session
import me.valex.botanic.models.User
import me.valex.botanic.network.Authorization
import me.valex.botanic.services.TimerService

class LoginViewModel(val context: Context,
                     var username: String = "",
                     var email: String = "",
                     var password: String = "") : BaseObservable() {

    @Bindable var loading: Boolean = false
    @Bindable var signIn: Boolean = true
    @Bindable var error: String = ""
    @Bindable var header: String = context.getString(if (signIn) R.string.sign_in else R.string.sign_up)
    @Bindable var signUpInSwitchText: String = context.getString(if (signIn) R.string.switch_to_sign_up else R.string.switch_to_sign_in)
    @Bindable var signUpInButtonText: String = context.getString(if (signIn) R.string.sign_in else R.string.sign_up)
    @Bindable var titleText: String = context.getString(if (!signIn) R.string.switch_to_sign_up else R.string.switch_to_sign_in)
    @Bindable var currentUser: User? = Database.getInstance(context).user

    init {
        BroadcastManager.subscribeTo(context, KEY_USER_CHANGED, { intent ->
            onUserChanged()
        })
        /*
        BroadcastManager.subscribeTo(context, KEY_TIME_CHANGED, { intent ->
            val seconds: Int = intent?.getIntExtra(EXTRA_SECONDS, 0) ?: 0
            timerTime = getTimerTimeText(seconds)
            notifyPropertyChanged(BR.timerTime)
        })

        */
    }

    fun onUserChanged() {
        currentUser = Database.getInstance(context).user
        notifyPropertyChanged(BR.currentUser)
    }

    fun setError_(error: String?) {
        if (error != null) {
            this.error = error
            notifyPropertyChanged(BR.error)
        }
    }

    fun setLoading_(loading: Boolean) {
        this.loading = loading
        notifyPropertyChanged(BR.loading)
    }

    fun setUsername_(username: String) {
        this.username = username
        setError_("")
    }

    fun setEmail_(email: String) {
        this.email = email
        setError_("")
    }

    fun setPassword_(password: String) {
        this.password = password
        setError_("")
    }

    fun onSingInClicked(view: View) {
        (view.context as AppCompatActivity).finish()
    }

    fun onSignOut(view: View) {
        Authorization.signOut()
        deleteUser(context)
        onUserChanged()

        val intent = Intent(context, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        context.startActivity(intent)
    }

    fun onShowProfile(view: View) {
        val intent = Intent(context, ProfileActivity::class.java)
        context.startActivity(intent)
    }

    var running: Boolean? = null

    fun onSkip(view: View) {
        if (getUser(context).hasOfflineAccount()) {
            val intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)
            return
        }

        setLoading_(true)

        Authorization.signInAnonym { success, error ->
            setLoading_(false)

            if (success) {
                onUserChanged()
            }
            else {
                setError_(error)
            }
        }
    }

    fun onSignUp(view: View) {

        if (username.isEmpty() && !signIn) {
            bounceView(childWithId(view, R.id.inputUsername))
            focus(childWithId(view, R.id.editUsername))
            setError_(context.getString(R.string.empty_name))
        }
        else if (!isEmail(email)) {
            bounceView(childWithId(view, R.id.inputEmail))
            focus(childWithId(view, R.id.editEmail))
            setError_(context.getString(R.string.wrong_email))
        }
        else if (password.isEmpty()) {
            bounceView(childWithId(view, R.id.inputPassword))
            focus(childWithId(view, R.id.editPassword))
            setError_(context.getString(R.string.empty_password))
        }
        else {
            setError_("")
            hideKeyboard(view)
            signUp()
        }
    }

    fun getUser(context: Context): User {
        currentUser = Database.getInstance(context).user
        return currentUser!!
    }

    fun signUp() {
        setLoading_(true)

        if (signIn) {
            Authorization.signIn(email, password, { success, error ->
                setLoading_(false)

                if (success) {
                    onUserChanged()
                }
                else {
                    setError_(error)
                }
            })
        }
        else {
            Authorization.signUp(username, email, password, { success, error ->
                setLoading_(false)

                if (success) {
                    onUserChanged()
                }
                else {
                    setError_(error)
                }
            })
        }
    }

    fun childWithId(parent: View?, id: Int): View? {
        return (parent?.parent as ViewGroup).findViewById(id)
    }

    fun onSignUpInSwitch(view: View) {
        signIn = !signIn
        notifyPropertyChanged(BR.signIn)

        if (!signIn) {
            focus(childWithId(view, R.id.editUsername))
        }

        signUpInSwitchText = context.getString(if (signIn) R.string.switch_to_sign_up else R.string.switch_to_sign_in)
        notifyPropertyChanged(BR.signUpInSwitchText)

        signUpInButtonText= context.getString(if (signIn) R.string.sign_in else R.string.sign_up)
        notifyPropertyChanged(BR.signUpInButtonText)

        titleText = context.getString(if (!signIn) R.string.switch_to_sign_up else R.string.switch_to_sign_in)
        notifyPropertyChanged(BR.titleText)

        setError_("")
    }

}