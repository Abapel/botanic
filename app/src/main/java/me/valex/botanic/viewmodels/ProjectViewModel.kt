package me.valex.botanic.viewmodels

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.graphics.Point
import android.support.v7.app.AppCompatActivity
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import com.github.ksoichiro.android.observablescrollview.ScrollUtils
import com.google.firebase.database.*
import me.valex.botanic.BR
import me.valex.botanic.R
import me.valex.botanic.activities.TaskActivity
import me.valex.botanic.adapters.BindingAdapters
import me.valex.botanic.extensions.*
import me.valex.botanic.models.*
import me.valex.botanic.network.Api
import me.valex.botanic.services.TimerService
import java.util.*

/**
 * Created by alex on 08.02.17.
 */

val MENU_ITEM_CHAT = 0
val MENU_ITEM_PIN = 1
val MENU_ITEM_TASKS = 2
val MENU_ITEM_TEAM = 3
val MENU_ITEM_GRAPH = 4

class ProjectViewModel(val context: Context,
                       val creatingProject: Boolean,
                       @Bindable val project: Project = Project(),
                       @Bindable var title: String = project.name,
                       var onMenuItemSelected: (item: Int) -> Unit = { _->}) : BaseObservable() {

    @Bindable var loading: Boolean = false
    @Bindable var error: String = ""

    @Bindable var headerTransitionY: Float = 0F

    @Bindable var timerTransitionY: Float = 0F
    @Bindable var timerAlpha: Float = 0F
    @Bindable var timerScaleY: Float = 1F
    @Bindable var timerState: Int = 0
    @Bindable var timerTime: Long = 0L

    @Bindable var flexibleSpaceImageHeight: Int = 0
    @Bindable var overlayAlpha: Float = 0F
    @Bindable var imageTransitionY: Float = 0F
    @Bindable var textScale: Float = 0F
    @Bindable var textTransition: Float = 0F

    @Bindable var chatBadge: String = ""
    @Bindable var pinBadge: String = ""
    @Bindable var tasksBadge: String = "!"
    @Bindable var graphBadge: String = ""
    @Bindable var teamBadge: String = "!"

    @Bindable var editingMessage: Boolean = false

    @Bindable var chatResId: Int = R.drawable.ic_chat
    @Bindable var pinResId: Int = R.drawable.ic_pin
    @Bindable var tasksResId: Int = R.drawable.ic_start
    @Bindable var graphResId: Int = R.drawable.ic_graph
    @Bindable var sendResId: Int = if (editingMessage) R.drawable.ic_edit else R.drawable.ic_send
    @Bindable var teamResId: Int = R.drawable.ic_team

    @Bindable var sliderWidth: Int
    @Bindable var pagerSliderTranslation: Int = 0

    @Bindable var currentMessage: Message = Message()
    @Bindable var currentSession: Session? = null
    @Bindable var currentTask: Task? = null

    val MAX_TEXT_SCALE_DELTA = 0.3f
    val MENU_ITEMS_COUNT = 5
    val screenWidth: Int
    val toolbarHeight: Int

    init {
        flexibleSpaceImageHeight = context.resources.getDimensionPixelSize(R.dimen.flexible_space_image_height)
        toolbarHeight = context.resources.getDimensionPixelSize(R.dimen.tab_bar_height)
        headerTransitionY = flexibleSpaceImageHeight.toFloat()

        val display = (context as Activity).windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        screenWidth = size.x
        sliderWidth = screenWidth / MENU_ITEMS_COUNT

        timerState = TIMER_STATE_RUNNING
        notifyPropertyChanged(BR.timerState)
    }

    fun _setEditing(editing: Boolean) {
        editingMessage = editing
        currentMessage.edited = editing
        notifyPropertyChanged(BR.editingMessage)
    }

    fun _setChatBadge(text: String) {
        chatBadge = text
        notifyPropertyChanged(BR.chatBadge)
    }

    fun _setTasksBadge(text: String) {
        tasksBadge = text
        notifyPropertyChanged(BR.tasksBadge)
    }

    fun onSettingsClicked(view: View) {
        log("onSettingsClicked")
    }

    fun setScrollX(scrollX: Int) {
        val ratio = scrollX.toFloat() / (MENU_ITEMS_COUNT * screenWidth)
        pagerSliderTranslation = (screenWidth * ratio).toInt()
        notifyPropertyChanged(BR.pagerSliderTranslation)
        log(pagerSliderTranslation)
    }

    fun getFlexibleRange() = flexibleSpaceImageHeight - toolbarHeight * 2

    fun getScrollRatio(scrollY: Int): Float {
        val flexibleRange = getFlexibleRange()
        val realScroll = flexibleRange - if (flexibleRange - scrollY < 0) 0 else flexibleRange - scrollY
        return realScroll.toFloat() / flexibleRange
    }

    fun getMaxTimerTransition(): Float = getFlexibleRange() / 2.7F

    fun onScrollChanged(scrollY: Int, firstScroll: Boolean, dragging: Boolean) {
        log("onScroll y=$scrollY firstScroll=$firstScroll dragging=$dragging")
        log("ratio=${getScrollRatio(scrollY)}")

        val minOverlayTransitionY = toolbarHeight * 2 - flexibleSpaceImageHeight //??

        headerTransitionY = ScrollUtils.getFloat((-scrollY).toFloat(), minOverlayTransitionY.toFloat(), 0f)
        notifyPropertyChanged(BR.headerTransitionY)

        imageTransitionY = ScrollUtils.getFloat((-scrollY / 2).toFloat(), minOverlayTransitionY.toFloat(), 0f)
        notifyPropertyChanged(BR.imageTransitionY)

        val ratio = getScrollRatio(scrollY)

        timerTransitionY = getMaxTimerTransition() * ratio
        notifyPropertyChanged(BR.timerTransitionY)

        timerAlpha = 1F * (1 - ratio)
        notifyPropertyChanged(BR.timerAlpha)

        timerScaleY = 1 - ratio
        notifyPropertyChanged(BR.timerScaleY)
    }

    fun setLoading_(loading: Boolean) {
        this.loading = loading
        notifyPropertyChanged(BR.loading)
    }

    fun setError_(error: String) {
        this.error = error
        notifyPropertyChanged(BR.error)
    }

    fun onNameChanged(newName: String) {
        project.name = newName
        project.save(context)
    }

    fun onAddTask() {
        TaskActivity.start(context, TaskActivity.MODE_CREATE, project)
    }

    val messagesEvents = object : ChildEventListener {

        override fun onCancelled(error: DatabaseError?) {
        }

        override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
        }

        override fun onChildAdded(dataSnapshot: DataSnapshot?, previousChildName: String?) {
            val message = dataSnapshot?.getValue(Message::class.java)
            if (message != null) {
                project.onMessageAdded(message)
                _setTasksBadge("${project.messages.size}")
                playChatSound(context)
            }
        }

        override fun onChildChanged(dataSnapshot: DataSnapshot?, p1: String?) {
            val message = dataSnapshot?.getValue(Message::class.java)
            if (message != null) {
                project.onMessageUpdated(message)
            }
        }

        override fun onChildRemoved(dataSnapshot: DataSnapshot?) {
            val message = dataSnapshot?.getValue(Message::class.java)
            if (message != null) {
                project.onMessageRemoved(message)
            }
        }
    }

    val tasksEvents = object : ChildEventListener {

        override fun onCancelled(error: DatabaseError?) {
        }

        override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
        }

        override fun onChildAdded(dataSnapshot: DataSnapshot?, previousChildName: String?) {
            val task = dataSnapshot?.getValue(Task::class.java)
            if (task != null) {
                project.onTaskAdded(task)
                _setTasksBadge("${project.tasks.size}")
            }
        }

        override fun onChildChanged(dataSnapshot: DataSnapshot?, p1: String?) {
            val task = dataSnapshot?.getValue(Task::class.java)
            if (task != null) {
            }
        }

        override fun onChildRemoved(dataSnapshot: DataSnapshot?) {
            val task = dataSnapshot?.getValue(Task::class.java)
            if (task != null) {
                project.onTaskRemoved(task)
            }
        }
    }

    var messagesReferece: DatabaseReference? = null
    var tasksReferece: DatabaseReference? = null
    var timeReceiver: BroadcastReceiver? = null

    fun subscribeForEvents(context: Context) {
        messagesReferece = Api.database.getReference("chats/${project.chatId}/messages")
        messagesReferece?.addChildEventListener(messagesEvents)

        tasksReferece = Api.database.getReference("users/${project.userId}/tasks")
        tasksReferece?.addChildEventListener(tasksEvents)

        timeReceiver = BroadcastManager.subscribeTo(context, KEY_TIMER_TIME, { intent ->
            if (intent != null && intent.hasExtra(EXTRA_TIMER_TIME)) {
                val time = intent.getLongExtra(EXTRA_TIMER_TIME, 0L)
                timerTime = time
                notifyPropertyChanged(BR.timerTime)
            }
        })
    }

    fun unsubscribeFromEvents() {
        messagesReferece?.removeEventListener(messagesEvents)
        tasksReferece?.removeEventListener(tasksEvents)

        if (timeReceiver != null) {
            BroadcastManager.unsubscribe(context, timeReceiver!!)
        }
    }

    fun onCreateProject(view: View) {

        if (project.name.isEmpty()) {
            setError_(context.getString(R.string.empty_project_name))
            bounceView((view.parent as ViewGroup).findViewById(R.id.inputName))
            return
        }

        setLoading_(true)

        Api.createProjectAndChat(context, project, { success, error ->
            setLoading_(false)
            if (success) {
                (context as Activity).finish()
            }
            else if (error != null && error.isNotEmpty()) {
                this.error = error
                notifyPropertyChanged(BR.error)
            }
        })
    }

    fun onDeleteProject(view: View) {
        Database.getInstance(context).delete(context, project)
    }

    fun _setCurrentMessage(text: String) {
        currentMessage.message = text
        notifyPropertyChanged(BR.currentMessage)
    }

    fun onCreateMessage(view: View) {
        if (editingMessage) {
            Api.updateMessage(context, project.chatId, currentMessage)
            _setEditing(false)
            _setCurrentMessage("")
            BindingAdapters.hideSoftKeyboard(context)
        }
        else if (currentMessage.message.isNotEmpty()) {
            val message = Message(
                    date = Calendar.getInstance().time,
                    username = getUser(context).email,
                    avatar = getUser(context).avatar,
                    message = currentMessage.message,
                    hasAttachments = false)
            Api.createMessage(context, project.chatId, message)
            _setCurrentMessage("")
            BindingAdapters.hideSoftKeyboard(context)
        }
        else {
            bounceView(view.parent as View)
        }
    }

    //
    //  SESSIONS WORKFLOW
    //

    fun onStartSession(task: Task) {
        if (currentSession == null) {

            currentTask = task
            notifyPropertyChanged(BR.currentTask)

            currentSession = Session(getUser(context))
            currentSession!!.state = Session.STATE_RUNNING
            notifyPropertyChanged(BR.currentSession)

            setLoading_(true)
            Api.createSession(context, project, task, currentSession!!, { success, key,  error ->
                setLoading_(false)
                logApi("$success $error")
            })

            TimerService.syncWith(context, currentSession!!)
        }
    }

    fun onStopSession() {
        updateSession(Session.STATE_DONE)
    }

    fun onPauseResumeSession(view: View) {
        if (currentSession != null) {
            val currentState = currentSession!!.state
            var newState = Session.STATE_PAUSED
            if (currentState == newState)
                newState = Session.STATE_RUNNING

            updateSession(newState)
        }
    }

    fun updateSession(state: Int) {
        if (currentSession != null && currentTask != null) {

            currentSession!!.state = state
            if (state == Session.STATE_DONE) {
                currentSession!!.ended = System.currentTimeMillis()
                currentSession!!.worked += System.currentTimeMillis() - currentSession!!.started
            }
            else if (state == Session.STATE_RUNNING) {
                currentSession!!.started = System.currentTimeMillis()
            }
            else if (state == Session.STATE_PAUSED) {
                currentSession!!.worked += System.currentTimeMillis() - currentSession!!.started
            }

            notifyPropertyChanged(BR.currentSession)

            setLoading_(true)
            Api.updateSession(context, project, currentTask!!, currentSession!!, { success, error ->
                setLoading_(false)
                logApi("$success $error")
            })
            TimerService.syncWith(context, currentSession!!)

            if (state == Session.STATE_DONE) {
                currentSession = null
                notifyPropertyChanged(BR.currentSession)

                currentTask = null
                notifyPropertyChanged(BR.currentTask)
            }
        }
    }

    /*
fun pauseSession(context: Context): Session? {
            setLoading_(true)
            currentSession!!.state = Session.STATE_RUNNING
            currentSession!!.resumed = System.currentTimeMillis()
            Api.updateSession(context, currentSession!!, { success, error ->
                setLoading_(false)
                logApi("$success $error")
            })
        }
        return currentSession!!
    if (currentSession != null) {
        currentSession!!.state = Session.STATE_PAUSED
        currentSession!!.logTime += System.currentTimeMillis() - currentSession!!.resumed!!
        setLoading_(true)
        Api.updateSession(context, currentSession!!, { success, error ->
            setLoading_(false)
            logApi("$success $error")
        })
    }
    notifyPropertyChanged(BR.currentSession)
    return currentSession
}


fun stopSession(context: Context) {
    if (currentSession != null) {
        currentSession!!.state = Session.STATE_DONE
        currentSession!!.ended = System.currentTimeMillis()
        currentSession!!.logTime += currentSession!!.ended!! - currentSession!!.resumed!!
        setLoading_(true)
        Api.updateSession(context, currentSession!!, { success, error ->
            setLoading_(false)
            logApi("$success $error")
        })

        currentSession = null
    }

    notifyPropertyChanged(BR.currentSession)
}
*/
    var _actionBarSize = -1
    fun getActionBarSize(): Int {
        if (_actionBarSize  == -1) {
            val typedValue = TypedValue()
            val textSizeAttr = intArrayOf(R.attr.actionBarSize)
            val indexOfAttrTextSize = 0
            val a = context.obtainStyledAttributes(typedValue.data, textSizeAttr)
            val actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1)
            a.recycle()
            _actionBarSize = actionBarSize
        }
        return _actionBarSize
    }
}

/*
val sessionEvents = object : ChildEventListener {
    override fun onChildMoved(dataSnapshot: DataSnapshot?, previousChildName: String?) {
        val session = dataSnapshot?.getValue(Session::class.java)
        logApi("[PROJECT MOVED: ${dataSnapshot?.key}] $session")
    }

    //New session has been added
    override fun onChildAdded(dataSnapshot: DataSnapshot?, previousChildName: String?) {
        val session = dataSnapshot?.getValue(Session::class.java)
        addSession(session)
    }

    //session changed
    override fun onChildChanged(dataSnapshot: DataSnapshot?, previousChildName: String?) {
        val session = dataSnapshot?.getValue(Session::class.java)
        updateSession(session)
    }

    //session removed
    override fun onChildRemoved(dataSnapshot: DataSnapshot?) {
        val session = dataSnapshot?.getValue(Session::class.java)
        removeSession(session)
    }

    //Api error
    override fun onCancelled(error: DatabaseError?) {
        logApi(error)
        logApi("API ERROR: ${error?.message}")
    }
}
*/