package me.valex.botanic.viewmodels

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.graphics.Color
import android.support.v4.content.LocalBroadcastManager
import android.view.View
import me.valex.botanic.BR
import me.valex.botanic.extensions.*
import me.valex.botanic.extensions.BroadcastManager.Companion.onSignedOut
import me.valex.botanic.models.Session
import me.valex.botanic.models.Task
import me.valex.botanic.services.TimerService
import java.util.*

/**
 * Created by alex on 04.02.17.
 */

class TaskViewModel(val context: Context,
                    @Bindable var task: Task,
                    @Bindable var time: String = "00:00",
                    @Bindable var running: Boolean = false,
                    @Bindable var color: Int = Color.rgb(random.nextInt(200), random.nextInt(200), random.nextInt(200)),
                    @Bindable var sessions: ArrayList<Session> = ArrayList()) : BaseObservable() {

    init {

    }

    fun onStartTask(view: View) {
        context.startService(Intent(context, TimerService::class.java))

        val session = Session(getUser(context))
        BroadcastManager.onSessionChanged(context, session)
    }

    fun onStopTask(view: View) {
        BroadcastManager.onSessionChanged(context, null)
    }
}