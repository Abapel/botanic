package me.valex.botanic.viewmodels

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.view.View
import me.valex.botanic.BR
import me.valex.botanic.R
import me.valex.botanic.extensions.getUser
import me.valex.botanic.extensions.log
import me.valex.botanic.models.Project
import me.valex.botanic.models.Session
import me.valex.botanic.network.Api
import me.valex.botanic.network.Api.Companion.createSession

class CreateSessionViewModel(val context: Context,
                             val project: Project,
                             val onSessionCreated:(session: Session) -> Unit,
                             @Bindable var loading: Boolean = true,
                             @Bindable var error: String = "") : BaseObservable() {

    fun setLoading_(loading: Boolean) {
        this.loading = loading
        notifyPropertyChanged(BR.loading)
    }

    fun setError_(error: String) {
        this.error = error
        notifyPropertyChanged(BR.error)
    }
}