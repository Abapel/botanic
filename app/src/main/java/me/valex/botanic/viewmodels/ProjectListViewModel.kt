package me.valex.botanic.viewmodels

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.view.View
import com.google.firebase.database.*
import me.valex.botanic.BR
import me.valex.botanic.R
import me.valex.botanic.activities.CreateProjectActivity
import me.valex.botanic.extensions.*
import me.valex.botanic.models.Project
import me.valex.botanic.models.Session
import me.valex.botanic.network.Api
import java.util.*


/**
 * Created by alex on 03.02.17.
 */

class ProjectListViewModel(val context: Context,
                           @Bindable var projects: ArrayList<Project> = ArrayList()) : BaseObservable() {

    @Bindable var session: Session? = null
    @Bindable var sessionTime: String = "00:00"
    @Bindable var sessionActive: Boolean = false
    @Bindable var sessionRunning: Boolean = false
    @Bindable var runningProject: Project? = null
    @Bindable var taskColor: Int = R.color.colorAccent

    init {

    }

    var receiver: BroadcastReceiver? = null
    var timeReceiver: BroadcastReceiver? = null

    fun setActive(active: Boolean) {
        sessionActive = active
        notifyPropertyChanged(BR.sessionActive)
    }

    fun setRunning(running: Boolean) {
        sessionRunning = running
        notifyPropertyChanged(BR.sessionRunning)
    }

    fun setProject(project: Project?) {
        runningProject = project
        notifyPropertyChanged(BR.runningProject)

        //taskColor = project?.color?.toInt() ?: Color.WHITE
        notifyPropertyChanged(BR.taskColor)

        setActive(true)
    }

    var projectsReferece: DatabaseReference? = null

    fun subscribeForEvents() {
        projectsReferece = Api.database.getReference("users/${getUser(context).getId_()}/projects")
        projectsReferece?.addChildEventListener(projectEvents)

        receiver = BroadcastManager.subscribeTo(context, KEY_PROJECT_CHANGED, { intent ->
            if (intent != null && intent.hasExtra(EXTRA_PROJECT)) {
                val json = intent.getStringExtra(EXTRA_PROJECT)
                setProject(Project.fromJson(json))
            }
        })
    }

    fun unsubscribeFromEvents() {
        projectsReferece?.removeEventListener(projectEvents)

        if (receiver != null) {
            BroadcastManager.unsubscribe(context, receiver!!)
        }

        if (timeReceiver != null) {
            BroadcastManager.unsubscribe(context, receiver!!)
        }
    }

    fun addProject(project: Project?) {
        if (project != null && !projectExist(project)) {
            projects.add(project)
            notifyPropertyChanged(BR.projects)
        }
    }

    fun updateProject(project: Project?) {
        val update = projects.find { it.id == project?.id }
        if (update != null && project != null) {
            val index = projects.indexOf(update)
            projects.removeAt(index)
            projects.add(index, project)
            notifyPropertyChanged(BR.projects)
        }
    }

    fun removeProject(project: Project?) {
        val remove = projects.find { it.id == project?.id }
        if (remove != null) {
            projects.remove(remove)
            notifyPropertyChanged(BR.projects)
        }
    }

    fun projectExist(project: Project?): Boolean {
        if (project != null) {
            projects.forEach {
                if (it.id == project.id) {
                    return true
                }
            }
        }
        return false
    }

    val projectEvents = object : ChildEventListener {
        override fun onChildMoved(dataSnapshot: DataSnapshot?, previousChildName: String?) {
            val project = dataSnapshot?.getValue(Project::class.java)
            logApi("[PROJECT MOVED: ${dataSnapshot?.key}] $project")
        }

        //New project has been added
        override fun onChildAdded(dataSnapshot: DataSnapshot?, previousChildName: String?) {
            val newProject = dataSnapshot?.getValue(Project::class.java)
            addProject(newProject)
        }

        //Project changed
        override fun onChildChanged(dataSnapshot: DataSnapshot?, previousChildName: String?) {
            val changedProject = dataSnapshot?.getValue(Project::class.java)
            updateProject(changedProject)
        }

        //Project removed
        override fun onChildRemoved(dataSnapshot: DataSnapshot?) {
            val removedProject = dataSnapshot?.getValue(Project::class.java)
            removeProject(removedProject)
        }

        //Api error
        override fun onCancelled(error: DatabaseError?) {
            logApi(error)
            logApi("API ERROR: ${error?.message}")
        }
    }

    fun onAddProject(view: View) {
        val intent = Intent(context, CreateProjectActivity::class.java)
        context.startActivity(intent)
    }
}