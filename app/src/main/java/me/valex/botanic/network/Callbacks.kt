package me.valex.botanic.network

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by alex on 25.01.17.
 */

class RetrofitCallback <T> (val onSuccess: (response: T) -> Unit, val onError: (reason: String?) -> Unit = {}) : Callback<T> {

    override fun onResponse(call: Call<T>?, response: Response<T>?) {
        if (response != null && response.body() != null) {
            onSuccess.invoke(response.body())
        }
        else {
            onError.invoke("DATA IS NULL")
        }
    }

    override fun onFailure(call: Call<T>?, t: Throwable?) = onError.invoke(t?.message)
}