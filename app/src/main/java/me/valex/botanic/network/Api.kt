package me.valex.botanic.network

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import me.valex.botanic.extensions.BroadcastManager
import me.valex.botanic.extensions.getUser
import me.valex.botanic.extensions.log
import me.valex.botanic.extensions.saveUser
import com.google.firebase.storage.FirebaseStorage
import me.valex.botanic.extensions.BroadcastManager.Companion.onNewMessage
import me.valex.botanic.models.*


class Api {

    companion object {

        private var context: Context? = null
        var authReference: FirebaseAuth = FirebaseAuth.getInstance()
        val database: FirebaseDatabase = FirebaseDatabase.getInstance()
        var storage:FirebaseStorage = FirebaseStorage.getInstance()
        var didInit: Boolean = false

        fun getInstance(context: Context): DatabaseReference {
            if (!didInit) {

                didInit = true
                this.context = context

                authReference.addAuthStateListener { auth ->
                    if (auth.currentUser != null) {
                        saveUser(context, auth.currentUser!!)
                        onConnected(context, true)
                    }
                }

                val database = FirebaseDatabase.getInstance()
                database.setPersistenceEnabled(true)

                val connectionReference = database.getReference(".info/connected")
                connectionReference.addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(error: DatabaseError?) {}
                    override fun onDataChange(snapshot: DataSnapshot?) {
                        val connected: Boolean? = snapshot?.getValue(Boolean::class.java)
                        if (connected != null) {
                            onConnected(context, connected)
                        }
                    }
                })
            }
            return database.reference
        }

        fun onConnected(context: Context, connected: Boolean) {
            val db = Database.getInstance(context)
            db.user.connected = connected
            db.save(context)

            BroadcastManager.onUserChanged(context)

            if (connected) {
                val userConnection = database.getReference("users/${getUser(context).getId_()}/connections")
                val userLastOnline = database.getReference("users/${getUser(context).getId_()}/lastOnline")

                val connection = userConnection.push()
                connection.setValue(true)
                connection.onDisconnect().removeValue()
                userLastOnline.onDisconnect().setValue(ServerValue.TIMESTAMP)
            }
        }

        fun createProjectAndChat(context: Context, project: Project, onDone: (success: Boolean, error: String?) -> Unit) {
            val projectsReference = database.getReference("users/${getUser(context).getId_()}/projects")
            val projectConnection = projectsReference.push()
            project.id = projectConnection.key
            project.userId = getUser(context).getId_()

            val chatsReference = database.getReference("chats")
            val chatsConnection = chatsReference.push()
            project.chatId = chatsConnection.key

            val chat = Chat(project.id)

            //Create chat api call
            chatsConnection.setValue(chat).addOnCompleteListener { result ->
                if (result.isSuccessful) {

                    //Create project api call
                    projectConnection.setValue(project).addOnCompleteListener { result ->
                        onDone.invoke(result.isSuccessful, result.exception?.message)
                    }
                }
            }
        }

        fun createTask(context: Context, project: Project, task: Task, onDone: (success: Boolean, error: String?) -> Unit = { _, _ ->}) {
            val taskReference = database.getReference("users/${project.userId}/projects/${project.id}/tasks")
            val taskConnection = taskReference.push()
            task.id = taskConnection.key

            taskConnection.setValue(task).addOnCompleteListener { result ->
                onDone.invoke(result.isSuccessful, result.exception?.message)
            }
        }

        fun createMessage(context: Context, chatId: String, message: Message, onDone: (success: Boolean, error: String?) -> Unit = { _, _ ->}) {
            val reference = database.getReference("chats/$chatId/messages")
            val connection = reference.push()
            message._id = connection.key
            connection.setValue(message).addOnCompleteListener { result ->
                onDone.invoke(result.isSuccessful, result.exception?.message)
            }
        }

        fun createSession(context: Context, project: Project, task: Task, session: Session, onDone: (success: Boolean, key: String, error: String?) -> Unit) {
            val sessionsReference = database.getReference("users/${project.userId}/projects/${project.id}/tasks/${task.id}/sessions")
            val sessionConnection = sessionsReference.push()
            session.id = sessionConnection.key
            sessionConnection.setValue(session).addOnCompleteListener { result ->
                onDone.invoke(result.isSuccessful, sessionConnection.key, result.exception?.message)
            }
        }

        fun updateSession(context: Context, project: Project, task: Task, session: Session, onDone: (success: Boolean, error: String?) -> Unit) {
            database.getReference("users/${project.userId}/projects/${project.id}/tasks/${task.id}/sessions/${session.id}")
                .setValue(session)
                .addOnCompleteListener { result ->
                    onDone.invoke(result.isSuccessful, result.exception?.message)
                }
        }

        fun updateMessage(context: Context, chatId: String, message: Message, onDone: (success: Boolean, error: String?) -> Unit = { _, _ ->}) {
            database.getReference("chats/$chatId/messages/${message._id}")
                    .setValue(message)
                    .addOnCompleteListener { result ->
                        onDone.invoke(result.isSuccessful, result.exception?.message)
                    }
        }

        fun removeMessage(context: Context, chatId: String, message: Message) {
            removeMessage(context, chatId, message, {success, error ->  })
        }

        fun removeMessage(context: Context, chatId: String, message: Message, onDone: (success: Boolean, error: String?) -> Unit) {
            database.getReference("chats/$chatId/messages/${message._id}")
                    .removeValue()
                    .addOnCompleteListener { result ->
                        onDone.invoke(result.isSuccessful, result.exception?.message)
                    }
        }

        /*
        fun getAllMessages(context: Context, allMessages: (messages: ArrayList<Message>) -> Unit) {
            val database = FirebaseDatabase.getInstance()
            val messagesReference = database.getReference("users/${getUser(context).getId_()}/messages")
            messagesReference.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError?) {}
                override fun onDataChange(snapshot: DataSnapshot?) {
                    val allMessages: ArrayList<Any>? = snapshot?.getValue(ArrayList::class.java)
                    if (allMessages != null) {

                    }
                }
            })
        }
        */

        fun getMessages(context: Context, onNewMessage: (message: Message) -> Unit) {
            val database = FirebaseDatabase.getInstance()
            val messagesReference = database.getReference("users/${getUser(context).getId_()}/messages")
            messagesReference.addChildEventListener(object : ChildEventListener {
                override fun onCancelled(error: DatabaseError?) {
                }

                override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
                }

                override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                }

                override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                }

                override fun onChildRemoved(p0: DataSnapshot?) {
                }
            })
        }
    }
}