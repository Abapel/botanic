package me.valex.botanic.network

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import com.google.gson.GsonBuilder

/**
 * Created by alex on 25.01.17.
 */

interface Network {

    @GET("data.json")
    fun getData() : Call<Any>

    companion object {
        val baseUrl = "https://botanic-8af3a.firebaseio.com/"

        private var _instance: Network? = null
        fun getInstance(): Network {
            if (_instance == null) {
                val gson = GsonBuilder()
                        .setLenient()
                        .create()
                val restAdapter = Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .baseUrl(baseUrl)
                        .build()
                _instance = restAdapter.create(Network::class.java)
            }
            return _instance!!
        }
    }
}