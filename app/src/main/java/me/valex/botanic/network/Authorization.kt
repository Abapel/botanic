package me.valex.botanic.network

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import me.valex.botanic.extensions.BroadcastManager
import me.valex.botanic.viewmodels.LoginViewModel

object Authorization {

    private var context: Context? = null
    private var loginModel: LoginViewModel? = null

    val auth: FirebaseAuth = FirebaseAuth.getInstance()

    fun startStateListener(context: Context, loginModel: LoginViewModel?) {
        this.context = context
        this.loginModel = loginModel
        //auth.addAuthStateListener(stateListener)
    }

    fun stopStateListener() {
        //auth.removeAuthStateListener(stateListener)
    }

    fun signUp(username: String, email: String, password: String, onDone: (success: Boolean, error: String?) -> Unit) {
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { result ->
                    onDone.invoke(result.isSuccessful, result.exception?.message)
                 }
    }

    fun signIn(email: String, password: String, onDone: (success: Boolean, error: String?) -> Unit) {
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { result ->
                    onDone.invoke(result.isSuccessful, result.exception?.message)
                }
    }

    fun signInAnonym(onDone: (success: Boolean, error: String?) -> Unit) {
        auth.signInAnonymously()
                .addOnCompleteListener { result ->
                    onDone.invoke(result.isSuccessful, result.exception?.message)
                }
    }

    fun signOut() {
        auth.signOut()
    }
}