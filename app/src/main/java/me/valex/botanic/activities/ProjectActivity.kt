package me.valex.botanic.activities

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import me.valex.botanic.R
import me.valex.botanic.adapters.ProjectPagerAdapter
import me.valex.botanic.databinding.ActivityProjectBinding
import me.valex.botanic.extensions.EXTRA_PROJECT
import me.valex.botanic.models.Project
import me.valex.botanic.viewmodels.ProjectViewModel
import me.valex.botanic.extensions.log
import me.valex.botanic.network.Api



class ProjectActivity : AppCompatActivity() {

    private var mHeaderView: View? = null
    private var mPager: ViewPager? = null
    private var mPagerAdapter: ProjectPagerAdapter? = null

    var model: ProjectViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (intent.hasExtra(EXTRA_PROJECT)) {
            val json = intent.getStringExtra(EXTRA_PROJECT)
            val project = Project.fromJson(json)
            val binder = DataBindingUtil.setContentView<ActivityProjectBinding>(this, R.layout.activity_project)
            model = ProjectViewModel(this, true, project, onMenuItemSelected = { item ->
                mPager?.setCurrentItem(item, true)
            })
            binder.model = model

            mHeaderView = findViewById(R.id.header)
            ViewCompat.setElevation(mHeaderView, resources.getDimension(R.dimen.toolbar_elevation))

            Api.getMessages(this, { newMessage ->
                model?.project?.onMessageAdded(newMessage)
            })

            model?.subscribeForEvents(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        model?.unsubscribeFromEvents()
    }

    companion object {
        val EXTRA_PROJECT = "project_json"

        fun startIntent(context: Context, project: Project) {
            val intent = Intent(context, ProjectActivity::class.java)
            intent.putExtra(EXTRA_PROJECT, project.toJson())
            context.startActivity(intent)
        }
    }
}
