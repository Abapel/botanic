package me.valex.botanic.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import me.valex.botanic.R
import me.valex.botanic.extensions.OnTextChanged
import me.valex.botanic.models.Project
import me.valex.botanic.models.Task
import me.valex.botanic.network.Api
import java.util.*

/**
 * Created by alex on 21.05.17.
 */

class TaskActivity : AppCompatActivity() {

    var editName: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)

        val mode = intent.getIntExtra(EXTRA_MODE, MODE_CREATE)

        val json = intent.getStringExtra(EXTRA_PROJECT)
        val project = Project.fromJson(json)

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(if (mode == MODE_CREATE) R.string.create_task else R.string.edit_task)

        val inputName =  findViewById(R.id.inputName) as TextInputLayout
        editName = findViewById(R.id.editName) as EditText
        inputName.isMotionEventSplittingEnabled = true

        val progressBar = findViewById(R.id.progressBar) as ProgressBar

        val btnCreate = findViewById(R.id.btnCreate) as Button
        btnCreate.setOnClickListener {
            onCreateTaskClicked(project)
        }
    }

    fun onCreateTaskClicked(project: Project) {
        if (checkInputFields()) {
            Api.createTask(this@TaskActivity, project, getCreatedTask(), { success, error ->
                finish()
            })
        }
    }

    fun checkInputFields(): Boolean {
        return true
    }

    fun getCreatedTask(): Task {
        val task = Task(editName?.text?.toString() ?: "")
        task.created = System.currentTimeMillis()
        return task
    }

    override  fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> saveAndExit()
        }
        return true
    }

    fun saveAndExit() {
        onBackPressed()
    }

    companion object {
        private val EXTRA_MODE = "EXTRA_MODE"
        private val EXTRA_PROJECT = "EXTRA_PROJECT"

        val MODE_CREATE = 1
        val MODE_EDIT = 2

        fun start(context: Context, mode: Int, project: Project) {
            val intent = Intent(context, TaskActivity::class.java)
            intent.putExtra(EXTRA_MODE, mode)
            intent.putExtra(EXTRA_PROJECT, project.toJson())
            context.startActivity(intent)
        }
    }
}