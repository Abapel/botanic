package me.valex.botanic.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.FrameLayout
import me.valex.botanic.R
import me.valex.botanic.adapters.MyPagerAdapter
import me.valex.botanic.fragments.CreatingSessionFragment
import me.valex.botanic.models.Project
import me.valex.botanic.models.Session
import me.valex.botanic.views.VerticalViewPager

class SessionActivity : AppCompatActivity() {

    var viewPager: VerticalViewPager? = null
    var fragmentContainer: FrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_session)

        val json = intent.getStringExtra("json")
        val project = Project.fromJson(json)

        val session: Session? = null

        if (session == null) {
            showCreatingSessionFragment(project)
        }
        else {
            showSessionPager(project, session)
        }

        val imgClose = findViewById(R.id.imgClose)
        imgClose.setOnClickListener {
            finish()
        }
    }

    fun showCreatingSessionFragment(project: Project) {
        fragmentContainer = findViewById(R.id.fragmentContainer) as FrameLayout
        val fragment = CreatingSessionFragment(project, { session ->
            showSessionPager(project, session)
        })
        fragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment).commit()
    }

    fun showSessionPager(project: Project, session: Session) {
        fragmentContainer?.visibility = View.GONE
        viewPager = findViewById(R.id.pager) as VerticalViewPager
        viewPager?.adapter = MyPagerAdapter(supportFragmentManager, project, session)
        viewPager?.currentItem = 0
    }

    companion object {
        fun startIntent(context: Context, project: Project) {
            val intent = Intent(context, SessionActivity::class.java)
            intent.putExtra("json", project.toJson())
            context.startActivity(intent)
        }
    }
}