package me.valex.botanic.activities

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.EditText
import me.valex.botanic.R
import me.valex.botanic.databinding.ActivityCreateProjectBinding
import me.valex.botanic.extensions.OnTextChanged
import me.valex.botanic.models.Project
import me.valex.botanic.viewmodels.ProjectViewModel

class CreateProjectActivity : AppCompatActivity() {

    var model: ProjectViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binder = DataBindingUtil.setContentView<ActivityCreateProjectBinding>(this, R.layout.activity_create_project)
        model = ProjectViewModel(this, true)
        binder.model = model

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(R.string.new_project)

        val inputName =  findViewById(R.id.inputName) as TextInputLayout
        val editName = findViewById(R.id.editName) as EditText
        inputName.isMotionEventSplittingEnabled = true
        editName.addTextChangedListener(OnTextChanged { newName ->
            model?.onNameChanged(newName)
        })
    }

    override  fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> saveAndExit()
        }
        return true
    }

    fun saveAndExit() {
        onBackPressed()
    }

    companion object {
        fun startIntent(context: Context, project: Project? = null) {
            val intent = Intent(context, CreateProjectActivity::class.java)
            if (project != null) {
                intent.putExtra("json", project.toJson())
            }
            context.startActivity(intent)
        }
    }
}