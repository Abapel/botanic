package me.valex.botanic.activities

import android.databinding.DataBindingUtil
import me.valex.botanic.R
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import me.valex.botanic.adapters.ProjectPagerAdapter
import me.valex.botanic.databinding.ActivityMainBinding
import me.valex.botanic.extensions.*
import me.valex.botanic.fragments.DrawerMainFragment
import me.valex.botanic.models.Project
import me.valex.botanic.viewmodels.ProjectListViewModel
import me.valex.botanic.viewmodels.ProjectViewModel
import me.valex.botanic.viewmodels.LoginViewModel
import android.view.Menu
import android.view.MenuItem
import me.valex.botanic.adapters.BindingAdapters
import me.valex.botanic.fragments.ProjectTasksFragment
import me.valex.botanic.models.Database
import java.util.*
import kotlin.concurrent.fixedRateTimer


class MainActivity : AppCompatActivity() {

    var allProjects: ProjectListViewModel? = null
    var loginModel: LoginViewModel? = null
    var projectModel: ProjectViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initViewModels()

        val binder = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binder.model = allProjects

        supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer, ProjectTasksFragment(allProjects)).commit()

        initDrawer()
        addGlobalHandlers()
    }

    override fun onResume() {
        super.onResume()

        loginModel?.currentUser = Database.getInstance(this).user
        loginModel?.onUserChanged()

        allProjects?.subscribeForEvents()
    }

    override fun onPause() {
        super.onPause()

        allProjects?.unsubscribeFromEvents()
    }

    fun initViewModels() {
        allProjects = ProjectListViewModel(this)
        loginModel = LoginViewModel(this)
        val last = Database.getInstance(this).lastProject
        if (last != null) {
            projectModel = ProjectViewModel(this, false, last)
            BroadcastManager.onProjectChanged(this, last)
        }
    }

    fun initDrawer() {
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        val drawer = findViewById(R.id.drawerLayout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        fragmentManager.beginTransaction().replace(R.id.drawerFragmentContainer, DrawerMainFragment(loginModel, allProjects)).commit()
    }

    fun addGlobalHandlers() {
        BroadcastManager.subscribeTo(this, KEY_SIGNED_IN, {
            loginModel?.onUserChanged()
        })

        BroadcastManager.subscribeTo(this, KEY_SIGNED_OUT, {
            loginModel?.onUserChanged()
        })
        BroadcastManager.subscribeTo(this, KEY_PROJECT_CHANGED, { intent ->
            val json = intent?.getStringExtra(EXTRA_PROJECT)
            if (json != null && json.isNotEmpty()) {
                val project = Project.fromJson(json)
                onProjectChanged(project)
            }
        })
    }

    fun onProjectChanged(project: Project) {
        val db = Database.getInstance(this)
        db.lastProject = project
        db.save(this)

        projectModel = ProjectViewModel(this, false, project)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_project, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.itemSettings -> {

                return true
            }
            R.id.itemDelete -> {


                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}
