package me.valex.botanic.activities

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import me.valex.botanic.R
import me.valex.botanic.adapters.BindingAdapters
import me.valex.botanic.databinding.ActivityLoginBinding
import me.valex.botanic.extensions.log
import me.valex.botanic.extensions.saveUser
import me.valex.botanic.models.Session
import me.valex.botanic.network.Api
import me.valex.botanic.services.TimerService
import me.valex.botanic.viewmodels.LoginViewModel
import java.io.DataOutputStream
import java.util.*
import kotlin.concurrent.fixedRateTimer

class LoginActivity : AppCompatActivity() {

    val authEvent = FirebaseAuth.AuthStateListener { auth ->
        if (auth.currentUser != null) {
            saveUser(this, auth.currentUser!!)
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    var loginModel: LoginViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
        loginModel = LoginViewModel(this)
        binding.model = loginModel

        Api.authReference.addAuthStateListener(authEvent)
    }

    override fun onResume() {
        super.onResume()
        loginModel?.onUserChanged()
    }

    override fun onDestroy() {
        super.onDestroy()
        Api.authReference.removeAuthStateListener(authEvent)
    }
}