package me.valex.botanic.models

/**
 * Created by alex on 19.05.17.
 */

class Chat(var projectId: String = "",
           var messages: ArrayList<Message> = ArrayList())