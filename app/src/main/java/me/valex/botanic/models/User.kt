package me.valex.botanic.models

import me.valex.botanic.extensions.log

/**
 * Created by alex on 08.02.17.
 */

class User (var name: String = "",
            var email: String = "",
            var id: String = "",
            var authorized: Boolean = false,
            var connected: Boolean = true,
            var offlineId: String = "",
            var avatar: String = "") {

    fun hasOfflineAccount(): Boolean {
        return offlineId.isNotEmpty()
    }

    fun getId_(): String {
        if (id.isNotEmpty()) {
            return id
        }
        else {
            return offlineId
        }
    }
}