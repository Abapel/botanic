package me.valex.botanic.models

import android.databinding.BaseObservable
import android.databinding.Bindable
import com.google.firebase.database.Exclude
import me.valex.botanic.BR
import me.valex.botanic.extensions.formatTime
import me.valex.botanic.extensions.gson

/**
 * Created by alex on 27.02.17.
 */

class Session(var user: User = User(),
              var id: String = "",
              var description: String = "",
              var created: Long = System.currentTimeMillis(),
              var started: Long = System.currentTimeMillis(),
              var ended: Long = 0L,
              var worked: Long = 0L) : BaseObservable() {

    @Bindable var state: Int = Session.STATE_PAUSED

    @get:Exclude @Bindable var error: String = ""
    @get:Exclude @Bindable var loading: Boolean = false
    @get:Exclude @Bindable var logTimeText: String = ""

    fun _getCurrentTime(): Long {
        var result = worked
        if (state == STATE_RUNNING) {
            result += System.currentTimeMillis() - started
        }
        return result
    }

    fun _isRunning(): Boolean = state == Session.STATE_RUNNING

    fun _getFormattedTime(): String {
        var millis = worked
        if (state == STATE_RUNNING && started != 0L) {
            millis += System.currentTimeMillis() - started
        }

        return formatTime(millis)
    }

    fun setLoading_(loading: Boolean) {
        this.loading = loading
        notifyPropertyChanged(BR.loading)

        if (loading) {
            setError_(null)
        }
    }

    fun setError_(error: String?) {
        this.error = error ?: ""
        notifyPropertyChanged(BR.error)
    }

    fun setState_(state: Int) {
        this.state = state
        notifyPropertyChanged(BR.state)
    }

    fun onUpdate() {
        notifyPropertyChanged(BR._all)
    }

    fun getTimeString(seconds: Long): String {
        val minutes = seconds / 60
        val hours = minutes / 60

        if (hours > 0) {
            return String.format("%02dч %02dм", hours, minutes)
        }
        else {
            return String.format("%02dм %02dс", minutes % 60, seconds % 60)
        }
    }

    fun toJson(): String {
        return gson.toJson(this)
    }

    companion object {
        //in file item_session.xml this value hardcoded to check equality
        val STATE_NOT_CREATED = -1
        val STATE_DONE = 0
        val STATE_RUNNING = 1
        val STATE_PAUSED = 2

        fun fromJson(json: String): Session? {
            if (json.isNotEmpty()) {
                return gson.fromJson(json, Session::class.java)
            }
            return null
        }
    }
}