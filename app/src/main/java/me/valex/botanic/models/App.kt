package me.valex.botanic.models

import android.app.Application
import me.valex.botanic.network.Api

/**
 * Created by alex on 05.03.17.
 */

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        Api.getInstance(this)
    }
}