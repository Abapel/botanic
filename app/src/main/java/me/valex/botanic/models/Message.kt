package me.valex.botanic.models

import me.valex.botanic.extensions.gson
import java.util.*

/**
 * Created by alex on 14.05.17.
 */

class Message(var username: String = "",
              var avatar: String = "",
              var message: String = "",
              var hasAttachments: Boolean = false,
              var _id: String = "",
              var date: Date = Date(),
              var edited: Boolean = false) {

    fun toJson(): String = gson.toJson(this)

    companion object {
        fun fromJson(json: String): Message {
            if (json.isNotEmpty()) {
                return gson.fromJson(json, Message::class.java)
            }
            return Message()
        }
    }
}