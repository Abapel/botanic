package me.valex.botanic.models

class Work(var started: Long = 0,
            var ended: Long = 0) {

    fun getWorked(): Long {
        return ended - started
    }
}
