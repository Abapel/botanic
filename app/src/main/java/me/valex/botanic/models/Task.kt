package me.valex.botanic.models

import android.databinding.BaseObservable
import android.view.View
import me.valex.botanic.extensions.gson
import me.valex.botanic.extensions.log
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by alex on 03.02.17.
 */

class Task(var name: String = "",
           var id: String = "",
           //var sessions: Map<String, Session> = HashMap(),
           var created: Long = System.currentTimeMillis(),
           var assignedUsers: ArrayList<User> = ArrayList()): BaseObservable() {

    /*
    fun isActive(): Boolean = timeWorked > 0

    fun getHours(): Int = timeWorked / (60 * 60 * 1000)

    fun getMinutes(): Int = timeWorked / 1000 / 60

    fun getSeconds(): Int = timeWorked / 1000 % 60
    */

    fun toJson(): String = gson.toJson(this)

    companion object {
        fun fromJson(json: String): Task = gson.fromJson(json, Task::class.java)
    }
}