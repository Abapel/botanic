package me.valex.botanic.models

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.Gson
import me.valex.botanic.extensions.gson
import java.util.*

/**
 * Created by alex on 23.02.17.
 */

class Database(var projects: ArrayList<Project> = ArrayList(),
               var lastProject: Project? = null,
               var user: User = User()) {

    fun toJson() : String = gson.toJson(this)

    fun save(context: Context) = getSharedPrefs(context).edit().putString(KEY, toJson()).apply()

    fun delete(context: Context, project: Project) {
        projects.remove(project)
        save(context)
    }

    fun insertOrReplace(context: Context, project: Project) {
        val oldProject = projects.find { it.id == project.id }
        if (oldProject != null) {
            val index = projects.indexOf(oldProject)
            projects[index] = project
        }
        else {
            projects.add(project)
        }
        save(context)
    }

    companion object {

        val KEY = "co_work_timer_database_key_7"

        private var db: Database? = null

        fun getSharedPrefs(context: Context): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

        fun fromJson(json: String): Database = Gson().fromJson(json, Database::class.java)

        fun getInstance(context: Context): Database {
            if (db == null) {
                val json = getSharedPrefs(context).getString(KEY, "")
                if (json.isNotEmpty()) {
                    db = Database.fromJson(json)
                } else {
                    db = Database()
                }
            }
            return db!!
        }
    }
}