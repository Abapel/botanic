package me.valex.botanic.models

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.view.View
import me.valex.botanic.BR
import me.valex.botanic.extensions.*
import kotlin.collections.ArrayList

/**
 * Created by alex on 06.02.17.
 */

class Project (var name: String = "",
               var id: String = "",
               var chatId: String = "",
               var userId: String = "",
               var created: Long = System.currentTimeMillis(),
               @Bindable var imageUrl: String = "", //""https://images.freecreatives.com/wp-content/uploads/2016/03/Colorful-Material-Design-Background.jpg",
               @Bindable var tasks: Map<String, Task> = HashMap(),
               @Bindable var messages: ArrayList<Message> = ArrayList(),
               @Bindable var colorScheme: ArrayList<String> = defaultColorScheme) : BaseObservable() {

    fun onTaskAdded(task: Task) {
        tasks = tasks.plus(Pair(task.id, task))
        notifyPropertyChanged(BR.tasks)
    }

    fun onTaskRemoved(task: Task) {
        tasks.minus(task.id)
        notifyPropertyChanged(BR.tasks)
    }

    //todo
    /*
    fun onTaskUpdated(task: Task) {
        val old = tasks.find { it.id == task.id }
        if (old != null) {
            val index = tasks.indexOf(old)
            tasks[index] = task
            notifyPropertyChanged(BR.tasks)
        }
    }
    */

    fun onMessageAdded(message: Message) {
        messages.add(0, message)
        notifyPropertyChanged(BR.messages)
    }

    fun onMessageRemoved(message: Message) {
        messages.remove(message)
        notifyPropertyChanged(BR.messages)
    }

    fun onMessageUpdated(message: Message) {
        val old = messages.find { it._id == message._id }
        if (old != null) {
            val index = messages.indexOf(old)
            messages[index] = message
            notifyPropertyChanged(BR.messages)
        }
    }

    fun onSelect(view: View) {
        BroadcastManager.onProjectChanged(view.context, this)
    }

    fun toJson(): String = gson.toJson(this)

    fun save(context: Context) {
        val db = Database.getInstance(context)
        db.insertOrReplace(context, this)
    }

    companion object {
        fun fromJson(json: String): Project = gson.fromJson(json, Project::class.java)
    }
}