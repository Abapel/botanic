package me.valex.botanic.services

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.IBinder
import me.valex.botanic.extensions.log
import java.util.*
import kotlin.concurrent.fixedRateTimer
import android.support.v7.app.NotificationCompat
import me.valex.botanic.R
import me.valex.botanic.activities.LoginActivity
import me.valex.botanic.extensions.BroadcastManager
import me.valex.botanic.extensions.EXTRA_SESSION
import me.valex.botanic.extensions.getLocalTime
import me.valex.botanic.models.Session

class TimerService : Service() {

    override fun onBind(p0: Intent?): IBinder? = null

    val NOTIFICATION_ID = 19091993
    var session: Session? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int{
        startForeground(NOTIFICATION_ID, getNotification())

        if (intent != null && intent.hasExtra(EXTRA_SESSION)) {
            val json = intent.getStringExtra(EXTRA_SESSION)
            session = Session.fromJson(json)
            when (session!!.state) {
                Session.STATE_RUNNING -> {
                    startTimer(true)
                }
                Session.STATE_PAUSED -> {
                    stopTimer()
                }
                Session.STATE_DONE -> {
                    stopTimer()
                    stopSelf()
                }
            }
        }

        return START_STICKY
    }

    fun getNotification(): Notification {
        //val notificationView = RemoteViews(packageName, R.layout.notification_timer)
        //notificationView.setTextViewText(R.id.textTime, timeText)

        val timeText = session?._getFormattedTime()

        val mBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_menu_camera)
                .setOngoing(false)
                .setContentText(timeText)
                //.setCustomBigContentView(notificationView)

        val resultIntent = Intent(this, LoginActivity::class.java)
        val resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        mBuilder.setContentIntent(resultPendingIntent)
        return mBuilder.build()
    }

    var timer: Timer? = null

    fun startTimer(useNotification: Boolean) {
        timer = fixedRateTimer(period = 333L, action = {

            if (useNotification) {
                getNotificationManager().notify(NOTIFICATION_ID, getNotification())
            }

            val time: Long
            if (session != null) {
                time = session!!._getCurrentTime()
            }
            else {
                time = getLocalTime()
            }

            BroadcastManager.onTimerTimeChanged(applicationContext, time)
        })
    }

    fun stopTimer() {
        timer?.cancel()
        timer = null
    }

    private var _notificationManager: NotificationManager? = null
    fun getNotificationManager(): NotificationManager {
        if (_notificationManager == null) {
            _notificationManager = getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager
        }
        return _notificationManager!!
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    companion object {
        fun syncWith(context: Context, session: Session) {
            val intent = Intent(context, TimerService::class.java)
            intent.putExtra(EXTRA_SESSION, session.toJson())
            context.startService(intent)
        }
    }
}