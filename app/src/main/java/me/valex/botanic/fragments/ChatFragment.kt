package me.valex.botanic.fragments

import android.content.BroadcastReceiver
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.valex.botanic.R
import me.valex.botanic.databinding.FragmentChatBinding
import me.valex.botanic.extensions.BroadcastManager
import me.valex.botanic.extensions.EXTRA_MESSAGE
import me.valex.botanic.extensions.KEY_NEW_MESSAGE
import me.valex.botanic.models.Message
import me.valex.botanic.viewmodels.ProjectViewModel

/**
 * Created by alex on 14.05.17.
 */

class ChatFragment(val project: ProjectViewModel? = null) : Fragment() {

    var receiver: BroadcastReceiver? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentChatBinding>(inflater, R.layout.fragment_chat, container, false)
        binding.project = project

        receiver = BroadcastManager.subscribeTo(activity, KEY_NEW_MESSAGE, { intent ->
            val json = intent?.getStringExtra(EXTRA_MESSAGE)
            if (json != null && json.isNotEmpty()) {
                val message = Message.fromJson(json)
                onNewMessage(message)
            }
        })

        return binding.root
    }

    fun onNewMessage(message: Message) {
        project?.project?.onMessageAdded(message)
    }

    override fun onDestroy() {
        if (receiver != null)
            BroadcastManager.unsubscribe(activity, receiver!!)
        super.onDestroy()
    }
}