package me.valex.botanic.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.valex.botanic.R
import me.valex.botanic.databinding.FragmentProjectTeamBinding
import me.valex.botanic.viewmodels.ProjectListViewModel
import me.valex.botanic.viewmodels.ProjectViewModel

/**
 * Created by alex on 09.02.17.
 */

class ProjectTeamFragment(val model: ProjectViewModel? = null) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentProjectTeamBinding>(inflater, R.layout.fragment_project_team, container, false)
        binding.model = model
        return binding.root
    }
}