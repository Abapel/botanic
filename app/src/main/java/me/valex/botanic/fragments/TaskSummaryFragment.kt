package me.valex.botanic.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.valex.botanic.R
import me.valex.botanic.activities.SessionActivity
import me.valex.botanic.databinding.FragmentTaskSummaryBinding
import me.valex.botanic.viewmodels.ProjectListViewModel

/**
 * Created by alex on 04.02.17.
 */

class TaskSummaryFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentTaskSummaryBinding>(inflater, R.layout.fragment_task_summary, container, false)
        return binding.root
    }
}