package me.valex.botanic.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.valex.botanic.R
import me.valex.botanic.databinding.FragmentSessionEditBinding

/**
 * Created by alex on 04.02.17.
 */

class TaskSettingsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentSessionEditBinding>(inflater, R.layout.fragment_session_edit, container, false)
        return binding.root
    }
}