package me.valex.botanic.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.valex.botanic.R
import me.valex.botanic.databinding.FragmentSessionTimerBinding
import me.valex.botanic.models.Project
import me.valex.botanic.models.Session

/**
 * Created by alex on 04.02.17.
 */

class SessionTimerFragment(val project: Project, val session: Session) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentSessionTimerBinding>(inflater, R.layout.fragment_session_timer, container, false)
        binding.project = project
        binding.session = session
        return binding.root
    }
}