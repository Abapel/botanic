package me.valex.botanic.fragments

import android.app.Fragment
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.valex.botanic.R
import me.valex.botanic.databinding.FragmentDrawerMainBinding
import me.valex.botanic.viewmodels.ProjectListViewModel
import me.valex.botanic.viewmodels.LoginViewModel

/**
 * Created by alex on 02.03.17.
 */

class DrawerMainFragment(val loginModel: LoginViewModel? = null, val allProjects: ProjectListViewModel? = null) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentDrawerMainBinding>(inflater, R.layout.fragment_drawer_main, container, false)
        binding.user = loginModel
        binding.allProjects = allProjects
        return binding.root
    }
}