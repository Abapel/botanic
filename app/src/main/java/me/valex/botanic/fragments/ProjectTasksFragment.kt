package me.valex.botanic.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.valex.botanic.R
import me.valex.botanic.databinding.FragmentProjectTasksBinding
import me.valex.botanic.viewmodels.ProjectListViewModel
import me.valex.botanic.viewmodels.ProjectViewModel

/**
 * Created by alex on 09.02.17.
 */

class ProjectTasksFragment(val project: ProjectListViewModel? = null) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentProjectTasksBinding>(inflater, R.layout.fragment_project_tasks, container, false)
        binding.model = project
        return binding.root
    }
}