package me.valex.botanic.fragments

import android.app.Fragment
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.valex.botanic.R
import me.valex.botanic.databinding.FragmentCreatingSessionBinding
import me.valex.botanic.models.Project
import me.valex.botanic.models.Session
import me.valex.botanic.viewmodels.CreateSessionViewModel

class CreatingSessionFragment(val project: Project, val onSessionCreated:(session: Session) -> Unit) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentCreatingSessionBinding>(inflater, R.layout.fragment_creating_session, container, false)
        binding.model = CreateSessionViewModel(activity, project, onSessionCreated)
        return binding.root
    }
}
