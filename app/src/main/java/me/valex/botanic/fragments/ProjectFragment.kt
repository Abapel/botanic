package me.valex.botanic.fragments

import android.app.Fragment
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.valex.botanic.R
import me.valex.botanic.adapters.ProjectPagerAdapter
import me.valex.botanic.databinding.ActivityProjectBinding
import me.valex.botanic.databinding.FragmentProjectBinding
import me.valex.botanic.models.Project
import me.valex.botanic.viewmodels.ProjectViewModel

/**
 * Created by alex on 03.03.17.
 */

class ProjectFragment(val model: ProjectViewModel? = null) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentProjectBinding>(inflater, R.layout.fragment_project, container, false)
        binding.model = model


        return binding.root
    }
}