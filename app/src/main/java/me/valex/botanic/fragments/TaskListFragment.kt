package me.valex.botanic.fragments

import android.app.Fragment
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.valex.botanic.R
import me.valex.botanic.databinding.FragmentTaskListBinding
import me.valex.botanic.viewmodels.ProjectListViewModel

/**
 * Created by alex on 03.02.17.
 */

class TaskListFragment(val model: ProjectListViewModel? = null) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentTaskListBinding>(inflater, R.layout.fragment_task_list, container, false)
        binding.model = model
        return binding.root
    }

}