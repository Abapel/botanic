package me.valex.botanic.fragments

import android.app.Fragment
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.valex.botanic.R
import me.valex.botanic.databinding.FragmentDrawerBinding
import me.valex.botanic.viewmodels.LoginViewModel

/**
 * Created by alex on 01.03.17.
 */

class DrawerFragment(val model: LoginViewModel? = null) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentDrawerBinding>(inflater, R.layout.fragment_drawer, container, false)
        binding.user = model
        return binding.root
    }
}