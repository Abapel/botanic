package me.valex.botanic.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.widget.Button
import me.valex.botanic.R

/**
 * Created by alex on 19.05.17.
 */

class ChatMessageDialog(val onChangeMessage: () -> Unit,
                        val onDeleteMessage: () -> Unit) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = activity.layoutInflater
        val builder = AlertDialog.Builder(activity)
        val view = inflater.inflate(R.layout.dialog_chat_message, null, false)

        val btnChange = view.findViewById(R.id.btnChange) as Button
        btnChange.setOnClickListener {
            onChangeMessage.invoke()
            dismiss()
        }

        val btnDelete = view.findViewById(R.id.btnDelete) as Button
        btnDelete.setOnClickListener {
            onDeleteMessage.invoke()
            dismiss()
        }

        builder.setView(view)
        return builder.create()
    }
}