package me.valex.botanic.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import me.valex.botanic.R
import me.valex.botanic.databinding.DialogSignUpBinding
import me.valex.botanic.extensions.bounceView
import me.valex.botanic.extensions.focus
import me.valex.botanic.extensions.isEmail
import me.valex.botanic.extensions.log
import me.valex.botanic.network.Authorization
import me.valex.botanic.viewmodels.LoginViewModel

/**
 * Created by alex on 27.02.17.
 */

class CreateProjectDialog(val model: LoginViewModel, val onDone: () -> Unit) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = activity.layoutInflater
        val builder = AlertDialog.Builder(activity)

        val binding = DataBindingUtil.inflate<DialogSignUpBinding>(inflater, R.layout.dialog_sign_up, null, false)
        binding.model = model

        builder.setView(binding.root)
        return builder.create()
    }
}