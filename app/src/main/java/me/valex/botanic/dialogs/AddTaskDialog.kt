package me.valex.botanic.dialogs


import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.widget.Button
import android.widget.EditText
import me.valex.botanic.R
import me.valex.botanic.extensions.bounceView
import me.valex.botanic.extensions.log
import me.valex.botanic.models.Task

/**
 * Created by alex on 27.02.17.
 */

class AddTaskDialog(val onDone: (result: Task) -> Unit) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = activity.layoutInflater
        val builder = AlertDialog.Builder(activity)

        val view = inflater.inflate(R.layout.dialog_new_task, null, false)

        val editName = view.findViewById(R.id.editName) as EditText
        val nameInput = view.findViewById(R.id.input) as TextInputLayout

        val btnCancel = view.findViewById(R.id.btnCancel) as Button
        btnCancel.setOnClickListener {
            dismiss()
            log("dismiss")
        }

        val btnCreate = view.findViewById(R.id.btnCreate) as Button
        btnCreate.setOnClickListener {
            log("create")
            val name = editName.text
            if (name.isEmpty()) {
                bounceView(nameInput)
            }
            else {
                val result = Task(name.toString())
                onDone.invoke(result)
                dismiss()
            }
        }

        builder.setView(view)
        return builder.create()
    }
}