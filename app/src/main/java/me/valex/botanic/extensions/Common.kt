package me.valex.botanic.extensions

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.Uri
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.google.gson.Gson
import me.valex.botanic.models.Project
import me.valex.botanic.models.Task
import java.util.*
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.google.firebase.analytics.FirebaseAnalytics
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import me.valex.botanic.R
import me.valex.botanic.models.Database
import me.valex.botanic.models.User
import me.valex.botanic.services.TimerService

val avatar1 = "https://pp.userapi.com/c10462/u14116007/128183484/x_06ea8ea8.jpg"

val defaultColorScheme = arrayListOf("#1C1F24", "#D75813", "AB0000")

private var user: User? = null
fun getUser(context: Context): User {
    if (user == null) {
        user = Database.getInstance(context).user
    }
    return user!!
}

val gson = Gson()

val handler = Handler()

val random = Random()

val logTag = "AppTag"
fun log(any: Any?) = System.out.println("$logTag: ${any?.toString()}")

val logApi = "AppTag"
fun logApi(any: Any?) = System.out.println("$logApi: ${any?.toString()}")

class OnTextChanged(val body: (newText: String) -> Unit) : TextWatcher {
    override fun afterTextChanged(p0: Editable?) {}
    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = body.invoke(p0.toString())
}

fun getTimerTimeText(seconds: Long): String {
    val hours = seconds / (60 * 60)
    val minutes = seconds / 60
    var result: String
    if (hours > 1) {
        result = String.format(String.format("%02d:%02d", hours, minutes))
    }
    else {
        result = String.format(String.format("%02d:%02d", minutes, seconds % 60))
    }
    return result
}

fun bounceView(view: View?) {

    val durations = longArrayOf(100, 90, 80, 70, 60, 50)
    val translations = floatArrayOf(50F, -40F, 30F, -20F, 10F, 0F)
    var currentIndex = 0

    fun bounce(view: View?) {
        view?.animate()?.translationX(translations[currentIndex])?.setDuration(durations[currentIndex])?.start()
        currentIndex++
        if (currentIndex < durations.size && currentIndex < translations.size) {
            handler.postDelayed({
                bounce(view)
            }, durations[currentIndex])
        }
    }
    bounce(view)
}

fun bounceTimer(textView: TextView?, colorTo: Int) {

    val durations = longArrayOf(100, 90, 80, 70, 60, 50)
    val sizes = floatArrayOf(1.15F, 0.8F, 1.1F, 0.85F, 1.05F, 0.95F)
    var currentIndex = 0

    if (textView != null) {
        val colorFrom = textView.textColors.defaultColor
        val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), colorFrom, colorTo)
        colorAnimation.addUpdateListener { animator ->
            textView.setTextColor(animator.animatedValue as Int)
        }
        colorAnimation.start()
    }

    fun bounce(view: View?) {
        view?.animate()?.scaleX(sizes[currentIndex])?.scaleY(sizes[currentIndex])?.setDuration(durations[currentIndex])?.start()
        currentIndex++
        if (currentIndex < durations.size && currentIndex < sizes.size) {
            handler.postDelayed({
                bounce(view)
            }, durations[currentIndex])
        }
    }
    bounce(textView)
}

fun isEmail(possibleEmail: String?): Boolean {
    return android.util.Patterns.EMAIL_ADDRESS.matcher(possibleEmail).matches()
}

fun focus(view: View?) {
    if (view != null && view is EditText && view.requestFocus()) {
        (view.context as AppCompatActivity).window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
    }
}

fun hideKeyboard(view: View?) {
    val inputManager = view?.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    inputManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
}

fun logEvent(context: Context, project: Project, task: Task) { // Created new task
    val bundle = Bundle()
    bundle.putString("Project", project.name)
    bundle.putString("Task", task.name)
    FirebaseAnalytics.getInstance(context).logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
}

fun logEvent(context: Context, task: Task, running: Boolean) { // Task started/stopped
    val bundle = Bundle()
    bundle.putString("Task", task.name)
    bundle.putBoolean("Running", running)
    FirebaseAnalytics.getInstance(context).logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
}

fun saveUser(context: Context, firebaseUser: FirebaseUser) {
    val db = Database.getInstance(context)
    db.user.authorized = !firebaseUser.isAnonymous
    db.user.email = firebaseUser.email ?: ""
    db.user.id = firebaseUser.uid
    if (!db.user.authorized) {
        db.user.offlineId = db.user.id
    }
    db.user.connected = true
    db.save(context)
}

fun deleteUser(context: Context) {
    val db = Database.getInstance(context)
    db.user.authorized = false
    db.user.email = ""
    db.user.id = ""
    db.user.name = ""
    db.save(context)
}

fun formatTime(timeMillis: Long): String {
    val allSecs = timeMillis / 1000
    val allMins = allSecs / 60
    val hrs = allMins / 60
    val mins = allMins - hrs * 60
    val secs = allSecs - allMins * 60

    if (hrs > 0L) {
        return String.format("%02d:%02d", hrs, mins)
    }
    return return String.format("%02d:%02d", mins, secs)
}

fun getLocalTime(): Long {
    val cal = Calendar.getInstance()
    val hrs = cal.get(Calendar.HOUR_OF_DAY)
    val mins = cal.get(Calendar.MINUTE)
    val secs = cal.get(Calendar.SECOND)
    val totalSecs = (hrs * 60 + mins) * 60 + secs
    return totalSecs * 1000L
}

private var mediaPlayer: MediaPlayer? = null
private fun getMediaPlayer(context: Context): MediaPlayer {
    if (mediaPlayer == null) {
        mediaPlayer = MediaPlayer.create(context, R.raw.chat_sound)
        mediaPlayer!!.setAudioStreamType(AudioManager.STREAM_NOTIFICATION)
        mediaPlayer!!.isLooping = false
    }
    return mediaPlayer!!
}

fun playChatSound(context: Context) {
    try {
        val r = RingtoneManager.getRingtone(context, Uri.parse("android.resource://${context.packageName}/${R.raw.chat_sound}"))
        r.play()
    } catch (ignored: Exception) {
    }
}

val TIMER_STATE_STOPPED = 0
val TIMER_STATE_PAUSED  = 1
val TIMER_STATE_RUNNING = 2