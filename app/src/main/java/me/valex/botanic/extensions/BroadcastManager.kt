package me.valex.botanic.extensions

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v4.content.LocalBroadcastManager
import me.valex.botanic.models.Message
import me.valex.botanic.models.Project
import me.valex.botanic.models.Session

val KEY_SIGNED_IN = "KEY_SIGNED_IN"
val KEY_SIGNED_OUT = "KEY_SIGNED_OUT"
val KEY_PROJECT_CHANGED = "KEY_PROJECT_CHANGED"
val KEY_PROJECT_ADDED = "KEY_PROJECT_ADDED"
val KEY_USER_CHANGED = "KEY_USER_CHANGED"
val KEY_USER_REMOVED = "KEY_USER_REMOVED"
val KEY_NEW_MESSAGE = "KEY_NEW_MESSAGE"
val KEY_SESSION_CHANGED = "KEY_SESSION_CHANGED"
val KEY_TIMER_TIME = "KEY_TIMER_TIME"

val EXTRA_TIMER_TIME = "EXTRA_TIMER_TIME"
val EXTRA_SESSION = "EXTRA_SESSION"
val EXTRA_PROJECT = "EXTRA_PROJECT"
val EXTRA_MESSAGE = "EXTRA_MESSAGE"

class BroadcastManager {
    companion object {

        fun onUserChanged(context: Context) {
            val broadcastIntent = Intent(KEY_USER_CHANGED)
            LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent)
        }

        fun onTimerTimeChanged(context: Context, time: Long) {
            val broadcastIntent = Intent(KEY_TIMER_TIME)
            broadcastIntent.putExtra(EXTRA_TIMER_TIME, time)
            LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent)
        }

        fun onSignedIn(context: Context?) {
            val broadcastIntent = Intent(KEY_SIGNED_IN)
            LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent)
        }

        fun onSignedOut(context: Context?) {
            val broadcastIntent = Intent(KEY_SIGNED_OUT)
            LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent)
        }

        fun onSessionChanged(context: Context?, session: Session?) {
            val broadcastIntent = Intent(KEY_SESSION_CHANGED)
            if (session != null) {
                broadcastIntent.putExtra(EXTRA_SESSION, session.toJson())
            }
            LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent)
        }

        fun onProjectChanged(context: Context?, project: Project) {
            val broadcastIntent = Intent(KEY_PROJECT_CHANGED)
            broadcastIntent.putExtra(EXTRA_PROJECT, project.toJson())
            LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent)
        }

        fun onProjectAdded(context: Context?, project: Project) {
            val broadcastIntent = Intent(KEY_PROJECT_ADDED)
            broadcastIntent.putExtra(EXTRA_PROJECT, project.toJson())
            LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent)
        }

        fun onNewMessage(context: Context?, message: Message) {
            val broadcastIntent = Intent(KEY_NEW_MESSAGE)
            broadcastIntent.putExtra(EXTRA_MESSAGE, message.toJson())
            LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent)
        }

        fun subscribeTo(context: Context, event: String, action: (intent: Intent?) -> Unit): BroadcastReceiver {
            val eventReceiver = object : BroadcastReceiver() {
                override fun onReceive(content: Context?, intent: Intent?) {
                    action.invoke(intent)
                }
            }
            LocalBroadcastManager.getInstance(context).registerReceiver(eventReceiver, IntentFilter(event))
            return eventReceiver
        }

        fun unsubscribe(context: Context, receiver: BroadcastReceiver) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver)
        }
    }
}